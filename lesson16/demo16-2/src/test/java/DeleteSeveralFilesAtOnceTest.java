import service.TestFile;
import service.TestFileFactory;
import screen.YandexDiskPage;
import service.Browser;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteSeveralFilesAtOnceTest extends BaseYDiskTest {

    public static final int TEST_FILES_QTY = 3;
    TestFile[] testFileN = new TestFile[TEST_FILES_QTY];

    @Test
    public void DeleteSeveralFilesAtOnceTest() {
        diskService.openRootFolder();
        testFileN = TestFileFactory.getTestFiles(TEST_FILES_QTY);
        for (int i = 0; i < TEST_FILES_QTY; i++) testFileN[i].storeTestFile();
        for (int i = 0; i < TEST_FILES_QTY; i++) {
            diskService.uploadFile(testFileN[i]);
        }
        Browser.current().waitForElementIsDisplayed(YandexDiskPage.ALL_UPLOADS_DONE);
        for (int i = 0; i < TEST_FILES_QTY; i++) diskService.selectMany(testFileN[i]);
        diskService.moveToTrash(testFileN[0]);
        diskService.waitForDeletingSomeFiles(TEST_FILES_QTY);
        boolean isFiles = true;
        for (int i = 0; i < TEST_FILES_QTY; i++) isFiles &= (!diskService.isTestFilePresent(testFileN[i]));
        Assert.assertTrue(isFiles);
    }
}
