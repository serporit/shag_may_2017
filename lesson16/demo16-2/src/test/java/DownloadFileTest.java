import service.TestFile;
import service.TestFileFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DownloadFileTest extends BaseYDiskTest {

    TestFile testFile = TestFileFactory.getTestFile();

    @Test
    public void DownloadFileTest() {
        testFile.storeTestFile();
        diskService.openRootFolder();
        diskService.uploadFile(testFile);
        testFile.deleteStoredTestFile();
        diskService.downloadFile(testFile);
        Assert.assertTrue(diskService.isTestFileDownloaded(testFile));
        testFile.deleteStoredTestFile();
    }
}
