import service.TestFileFactory;
import service.DiskService;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import service.Browser;
import service.YDLoginService;

public class BaseYDiskTest {

    DiskService diskService = new DiskService();

    @BeforeSuite
    public void prepeareForTests() {

        YDLoginService loginService = new YDLoginService();
        loginService.loginToYandexDisk();
        pause();
    }

    @AfterSuite
    public void closeWebdriver() {
        //Browser.current();
    }

    protected void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }
}
