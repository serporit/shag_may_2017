package service;

import service.TestFile;
import screen.YandexDiskPage;
import service.Browser;
import org.openqa.selenium.By;

import java.io.IOException;

public class DiskService {
    YandexDiskPage page = new YandexDiskPage();

    public void uploadFile(TestFile testFile) {
        page.uploadFile(testFile);
        pause();
    }

    public void downloadFile(TestFile testFile) {
        page.downloadFile(By.xpath("//*[@title='" + testFile.getTestFileName() + "']"));
        pause();
    }

    public boolean isTestFilePresent(TestFile testFile) {
        return page.isTestFilePresent(testFileLocator(testFile));
    }

    public boolean isTestFileDownloaded(TestFile testFile) {
        boolean fileDownloaded = false;
        try {
            fileDownloaded = TestFile.isTestFileStored(testFile);
        } catch (IOException e) {
        }
        return fileDownloaded;
    }

    public void moveToTrash(TestFile testFile) {
        page.moveToTrash(testFileLocator(testFile));
        pause();pause();
    }

    public void selectMany(TestFile testFile) {
        page.selectMany(testFileLocator(testFile));
    }

    public void restoreFromTrash(TestFile testFile) {
        page.restoreFromTrash(testFileLocator(testFile));
        pause();
    }

    public void permanentDelete(TestFile testFile) {
        page.permanentDelete(testFileLocator(testFile));
        pause();
    }

    public void openTrashBin() {
        page.openTrashBin();
        pause();
    }

    public void openRootFolder() {
        page.openRootFolder();
        pause();
    }

    private By testFileLocator(TestFile testFile) {
        return By.xpath(".//*[@title='" + testFile.getTestFileName() + "']");
    }

    public void waitForDeletingSomeFiles(int howManyFiles) {
    By locator=By.xpath("//*[contains(text(),'"+howManyFiles+" Файл']");
        Browser.current().waitForElementIsDisplayed(locator);
    }


    private void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }
}
