package service;

import org.apache.commons.lang3.RandomStringUtils;

public class TestFileFactory {

    public final static int TEST_FILENAME_LENGTH =8;
    public final static int TEST_CONTENT_LENGTH =30;

    public static TestFile getTestFile() {
        String fileName = "test_file_" + RandomStringUtils.randomAlphanumeric(TEST_FILENAME_LENGTH) + ".txt";
        String content = "test content " + RandomStringUtils.randomAlphanumeric(TEST_CONTENT_LENGTH);
        return new TestFile(fileName, "", content);
    }

    public static TestFile[] getTestFiles(int qty) {
        TestFile[] testFileN = new TestFile[qty];
        for (int i = 0; i < qty; i++) {
            String fileName = "test_file_" + RandomStringUtils.randomAlphanumeric(TEST_FILENAME_LENGTH) + ".txt";
            String content = "test content " + RandomStringUtils.randomAlphanumeric(TEST_CONTENT_LENGTH);
            testFileN[i] = new TestFile(fileName, "", content);
        }
        return testFileN;
    }
}
