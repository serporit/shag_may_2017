package service;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class Browser {

    private static Browser browser;
    private static WebDriver driver;
    public  final static int COMMON_ELEMENT_WAIT_TIME_OUT = 10;
    public  final static String YDisk_URL = "http:\\disk.yandex.ru";
//    private static DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

    private Browser() {
    }

    public static Browser current() {
        if (browser==null) {browser = new Browser();}
        return browser;
    }

    public WebDriver getWrappedDriver() {
        if (driver == null) startBrowser();
        return driver;
    }

    private static void startBrowser() {
        System.setProperty("webdriver.chrome.driver", "d:\\ОБМЕН\\AT 09-07-17\\demo16-2\\src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(YDisk_URL);
    }

    public static void stopBrowser() {
        driver.close();
    }

    public void waitForElementIsClickable(By locator) {
        try {
            new WebDriverWait(driver, COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.elementToBeClickable(locator));
        } catch (TimeoutException e) {
            pause();
        }
        screenshot();
    }

    public void waitForElementIsDisplayed(By locator) {
        try {
            new WebDriverWait(driver, COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            pause();
        }
        screenshot();
    }

    public void waitForDisappear(By locator) {
        try {
            new WebDriverWait(driver, COMMON_ELEMENT_WAIT_TIME_OUT)
                    .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            pause();
        }
        screenshot();
    }

    public static void screenshot() {
//            try {
//                byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
//                File screenshotFile = new File(screenshotName());
//                FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
//            } catch (Exception e) {
//            }
    }

    public void waitForAppear(By locator) {
        new WebDriverWait(driver, COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        screenshot();
    }

    public void writeText(By locator, String text) {
        //new Actions(driver).sendKeys(driver.findElement(locator),text).perform();
        driver.findElement(locator).sendKeys(text);
        screenshot();
    }

    public void writeTextByAction(By locator, String text) {
        new Actions(driver).sendKeys(driver.findElement(locator), text).perform();
        screenshot();
    }

    public void click(By locator) {
        new Actions(driver).click(driver.findElement(locator)).perform();
        screenshot();
    }

    public void doubleClick(By locator) {
        new Actions(driver).doubleClick(driver.findElement(locator)).perform();
        screenshot();
    }


    public String getText(By locator) {
        return driver.findElement(locator).getText();
    }

    public void clear(By locator) {
        driver.findElement(locator).clear();
    }

    public void submit(By locator) {
        driver.findElement(locator).submit();
    }

    public void dragTo(By draggable, By target) {
        new Actions(driver).dragAndDrop(driver.findElement(draggable), driver.findElement(target)).perform();
    }

    public void clickWithCTRL(By locator) {
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL).click(driver.findElement(locator)).build().perform();
    }

    private void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }

    public void openURL(String url) {
        getWrappedDriver().get(url);
    }

}
