package screen;

import service.TestFile;
import service.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import service.TestFile;

import java.util.List;

public class YandexDiskPage {

    public static final String MAIN_PAGE_URL = "https://disk.yandex.ru/client/disk";
    public static final By TRASH_LOCATOR = By.xpath(".//*[@title='Корзина']");
    public static final By RESTORE_BUTTON_LOCATOR = By.xpath(".//*[@data-click-action='resource.restore']");
    public static final By PERMANENT_DELETE_BUTTON_LOCATOR = By.xpath(".//*[@data-click-action='resource.delete']");
    //public static final By UPLOAD_LOCATOR = By.xpath("//input[@type='file']");
    public static final By UPLOAD_LOCATOR = By.xpath("//input[@class='upload-button__attach']");

    public static final By UPLOAD_POPUP_CLOSE = By.xpath("//*[@data-click-action='dialog.close']");
    public static final By ALL_UPLOADS_DONE = By.xpath("//*[text()='Все загрузки завершены']");
    public static final By DOWNLOAD_BUTTON = By.xpath("//button[@title='Скачать']");

    public void uploadFile(TestFile testFile) {
        Browser.current().writeText(UPLOAD_LOCATOR, testFile.getTestFilePath());
        Browser.current().waitForElementIsClickable(UPLOAD_POPUP_CLOSE);
        Browser.current().click(UPLOAD_POPUP_CLOSE);
        Browser.current().waitForElementIsDisplayed(ALL_UPLOADS_DONE);
    }

    public void downloadFile(By locator) {
        Browser.current().click(locator);
        Browser.current().waitForElementIsClickable(DOWNLOAD_BUTTON);
        Browser.current().click(DOWNLOAD_BUTTON);
    }

    public boolean isTestFilePresent(By testFile) {
        List<WebElement> search = Browser.current().getWrappedDriver().findElements(testFile);
        int i = search.size();
        if (0 == i) return false;
        for (WebElement elementN : search) if (elementN.isDisplayed()) return true;
        return false;
    }

    public void moveToTrash(By fileToTrash) {
        Browser.current().dragTo(fileToTrash, TRASH_LOCATOR);
    }

    public void restoreFromTrash(By restoredFile) {
        Browser.current().click(restoredFile);
        Browser.current().waitForElementIsClickable(RESTORE_BUTTON_LOCATOR);
        Browser.current().click(RESTORE_BUTTON_LOCATOR);
        pause();
    }

    public void permanentDelete(By erasedFile) {
        Browser.current().click(erasedFile);
        Browser.current().waitForElementIsClickable(PERMANENT_DELETE_BUTTON_LOCATOR);
        Browser.current().click(PERMANENT_DELETE_BUTTON_LOCATOR);
        pause();
    }

    public void selectMany(By locator) {
        Browser.current().clickWithCTRL(locator);
    }

    public void openTrashBin() {
        Browser.current().doubleClick(TRASH_LOCATOR);
    }

    public void openRootFolder() {
        Browser.current().openURL(MAIN_PAGE_URL);
    }

    private void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }


}


