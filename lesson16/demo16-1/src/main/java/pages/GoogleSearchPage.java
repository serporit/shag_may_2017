package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class GoogleSearchPage {

    private WebDriver driver;
    private static final String SEARCH_URL = "https://google.by/";
    private static final By INPUT_FIELD_LOCATOR = By.id("lst-ib");
    private static final By UL_LOCATOR = By.xpath("//li[@role='presentation']//div[@class='sbqs_c']");


    public GoogleSearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public GoogleSearchPage open() {
        driver.get(SEARCH_URL);
        return new GoogleSearchPage(driver);
    }

    public String getUrl() {
        return SEARCH_URL;
    }

    public List<String> getTipsList (List<String> cities) throws InterruptedException {
        List<String> allRequest = new ArrayList<String>();
        for (String city : cities) {
            driver.get(SEARCH_URL);
            driver.findElement(INPUT_FIELD_LOCATOR).sendKeys(city);
            Thread.sleep(500);
            driver.findElement(INPUT_FIELD_LOCATOR).submit();
            Thread.sleep(500);
            driver.findElement(INPUT_FIELD_LOCATOR).click();
            List<WebElement> tipsList = driver.findElements(UL_LOCATOR);
            for (WebElement line : tipsList) {
                allRequest.add(line.getText());
            }
        }
        return allRequest;
    }

}
