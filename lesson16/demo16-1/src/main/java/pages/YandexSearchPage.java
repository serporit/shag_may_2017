package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class YandexSearchPage {
    private WebDriver driver;
    private static final String SEARCH_URL = "https://yandex.by/";
    private static final By INPUT_FIELD_LOCATOR = By.id("text");
    private static final By UL_LOCATOR = By.xpath("//div[@class='popup__content']/ul/li/span");

    public YandexSearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public YandexSearchPage open() {
        driver.get(SEARCH_URL);
        return new YandexSearchPage(driver);
    }

    public String getUrl() {
        return SEARCH_URL;
    }

    public List<String> getTipsList (List<String> cities) throws InterruptedException {
        List<String> allRequest = new ArrayList<String>();
        for (String city : cities) {
            driver.get(SEARCH_URL);
            Actions actions = new Actions(driver);
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(INPUT_FIELD_LOCATOR));
            actions.sendKeys(driver.findElement(INPUT_FIELD_LOCATOR), city).build().perform();
            Thread.sleep(1000);
            List<WebElement> tipsList = driver.findElements(UL_LOCATOR);
            for (WebElement line : tipsList) {
                allRequest.add(line.getText());
            }
        }
        return allRequest;
    }

}
