package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.LinkedList;
import java.util.List;

public class CitiesListPage {
    private WebDriver driver;
    private static final String SEARCH_URL = "http://xn----7sbiew6aadnema7p.xn--p1ai/";
    private static final By CITY_LOCATOR = By.xpath("//tbody//a[contains(@href,'sity_id')]");

    public static List<String> getCitiesList(WebDriver driver) {
        List<String> cities = new LinkedList<String>();
        driver.get(SEARCH_URL);
        for (WebElement element: driver.findElements(CITY_LOCATOR)) {
         cities.add(element.getText());
        }
        return cities;
    }

}
