package worker;

import java.util.*;

public class Utility implements Comparator<String> {
    private static final String[] exclude = {"погода", "время", "работа", "валют", "из гомеля", "сегодня", "новости", "из минска", "на поезде"};

    public int compare(String s1, String s2) {
        return s2.length() - s1.length();
    }

     static List<String> changeCityNames(List<String> names) {
        List<String> changedNames = new LinkedList<String>();
        for (String name : names) {
            changedNames.add("в " + replaceLastLetter(name));
        }
        return changedNames;
    }

     static List<String> filter(List<String> frases) {
        List<String> resultList = new LinkedList<String>();
        List<String> filteredFrases = Arrays.asList(exclude);
        Boolean crapFraze;
        for (String frase : frases) {
            crapFraze = false;
            for (String filter : filteredFrases) {
                if (frase.contains(filter)) {
                    crapFraze = true;
                    break;
                }
            }
            if (!crapFraze) {
                resultList.add(frase);
            }
        }
        return resultList;
    }

     static void print(List<String> frases) {
        ArrayList<String> sorted = new ArrayList<String>(frases);
        Utility uu = new Utility();
        Collections.sort(sorted, uu);
        for (String line : sorted) {
            System.out.println(line);
        }
    }

    private static String replaceLastLetter(String name) {
        String response;
        int lastIndex = name.length() - 1;
        char lastLetter = name.charAt(lastIndex);
        switch (lastLetter) {
            case 'а': {
                response = name.substring(0, lastIndex) + "e";
                break;
            }
            case 'ь': {
                response = name.substring(0, lastIndex) + "и";
                break;
            }
            case 'о': {
                response = name.substring(0, lastIndex) + "е";
                break;
            }
            case 'и': {
                response = name;
                break;
            }
            default: {
                response = name + "е";
                break;
            }
        }
        return response;
    }

}
