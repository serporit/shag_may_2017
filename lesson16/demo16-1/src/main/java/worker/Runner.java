package worker;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.CitiesListPage;
import pages.GoogleSearchPage;
import pages.YandexSearchPage;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Runner {

    private WebDriver driver;
    public List<String> funniestTips = new LinkedList<String>();

    @BeforeTest
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterTest
    public void teardown() {
        driver.close();
    }

    @Test
    public void lookForFunnyTips() throws IOException, InterruptedException {
        List<String> citiesNames = Utility.changeCityNames(CitiesListPage.getCitiesList(driver));
        YandexSearchPage firstSearch = new YandexSearchPage(driver);
        funniestTips.addAll(Utility.filter(firstSearch.open().getTipsList(citiesNames)));
        GoogleSearchPage secondSearch = new GoogleSearchPage(driver);
        funniestTips.addAll(Utility.filter(secondSearch.open().getTipsList(citiesNames)));
        Utility.print(funniestTips);
    }




}
