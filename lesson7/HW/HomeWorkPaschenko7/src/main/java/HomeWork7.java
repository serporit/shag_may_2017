import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by User on 07.06.2017.
 */
public class HomeWork7 {
    public static void main(String[] args) throws IOException {
        String allFilesContent = "";
        File folder = new File("src\\main\\resources\\books");
        File[] files = folder.listFiles();
        for (File file : files) {
            String content = FileUtils.readFileToString(file);
            allFilesContent = allFilesContent + content;
        }
        List<String> listOfAllFilesContent = Arrays.asList(allFilesContent.split("[\\n\\r:;,\\(\\)№#\\._'\"\\s\\-\\d]+"));
        Set<String> uniqueWords = new HashSet<String>(listOfAllFilesContent);
        for (String word : uniqueWords) {
            System.out.println(word + ": " + Collections.frequency(listOfAllFilesContent, word));
        }
    }
}
