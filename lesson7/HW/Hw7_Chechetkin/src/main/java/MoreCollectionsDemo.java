import java.util.HashMap;
import java.util.Map;

public class MoreCollectionsDemo {
    public static void main(String[] args) {
        String student1 = "Ivanov";
        String student2 = "Petrov";
        String student3 = "Sidorov";

        int mark1 = 7;
        int mark2 = 9;
        int mark3 = 5;

        Map<String, Integer> map = new HashMap<String, Integer>();

        map.put(student1, mark1);
        map.put(student2, mark2);
        map.put(student3, mark3);
        map.put("Ivanov", 15);
        System.out.println(map);

        System.out.println(map.get("QWe"));


    }
}
