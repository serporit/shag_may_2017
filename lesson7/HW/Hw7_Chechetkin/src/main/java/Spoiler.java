import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by User on 07.06.2017.
 */
public class Spoiler {
    public static void main(String[] args) throws IOException {

        File inputFile = new File("src\\main\\resources\\books\\babayka.txt");
        String fileContent = FileUtils.readFileToString(inputFile);
        String[] words = fileContent.split("[\\n\\r:;,\\(\\)№#\\._'\"\\s\\-\\d]+");


        String allFilesContent = "";

        File folder = new File("src\\main\\resources\\books");
        File[] files = folder.listFiles();

        for (File file : files) {
            String content = FileUtils.readFileToString(file);
            allFilesContent = allFilesContent + content;

        }
        String[] wordsFromAllFiles = allFilesContent.split("[\\n\\r:;,\\(\\)№#\\._'\"\\s\\-\\d]+");

        Map<String, Integer> mapWords = new TreeMap<String, Integer>();
        for (String outString: wordsFromAllFiles) {
            if (mapWords.get(outString) == null){
                mapWords.put(outString, 1);
            } else {
                mapWords.put(outString, mapWords.get(outString) + 1);
            }
        }
        for (Map.Entry<String, Integer> outStrings : mapWords.entrySet()) {
            System.out.println(outStrings.getKey() + " => " + outStrings.getValue());
        }
    }
}
