import java.io.IOException;
import java.util.Map;

public class Runner {
    public static void main(String[] args) throws IOException {
        String pathFolder = new ReaderConsole().readConsolePathForFolder();
        Parser parser = new Parser();
        FolderReader folderReader = new FolderReader();
        Map<String, Integer> sortedMapByFrequency = ValueComparator.sortByValue(parser.parse(folderReader.readFolder(pathFolder)));
        new DisplayMapToConsole().display(sortedMapByFrequency);
    }
}
//"G:\\10. Программирование\\QA\\Home Work\\Viktor HomeTaskMaven2\\src\\main\\resources\\books"