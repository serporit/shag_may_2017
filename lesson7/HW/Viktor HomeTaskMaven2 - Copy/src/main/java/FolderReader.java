import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class FolderReader {

    public String readFolder(String pathFolder) throws IOException {

        String allFilesContent = null;

        File folder = new File(pathFolder);
        File[] files = folder.listFiles();

        for (File file : files) {

            String content = FileUtils.readFileToString(file);
            allFilesContent = allFilesContent + content;
        }
        return allFilesContent;
    }
}
