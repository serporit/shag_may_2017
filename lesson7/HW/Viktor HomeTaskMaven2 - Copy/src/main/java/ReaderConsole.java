import java.io.File;
import java.util.Scanner;

public class ReaderConsole {

    public String readConsolePathForFolder() {

        String path = null;
        boolean check = false;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите абсолютный путь к дериктории с .txt файлами");

        while (!check) {
            if (scanner.hasNextLine()) {
                path = scanner.nextLine();

                File newFile = new File(path);

                if (newFile.isDirectory()) {

                    File listFile[] = newFile.listFiles();

                    int counter = 0;
                    for (File temp : listFile) {

                        counter++;
                        if (temp.isFile()) {
                            check = true;
                        } else if (counter == listFile.length || check == true) {
                            System.out.println("Данная дериктория не имеет .txt файлов. Введите другой путь");
                        }
                    }
                } else {
                    System.out.println("Такой директории не существует. Введите другой путь.");
                }
            } else {
                System.out.println("Некорректные данные. Введите путь к необходимому файлу");
                scanner.next();
            }
        }
        return path;
    }
}
