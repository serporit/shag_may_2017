import java.util.Map;

public class DisplayMapToConsole {

    public void display(Map<String, Integer> map){

    int counter = 0;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            counter++;
            System.out.println(counter + " " + entry);

        }
    }
}
