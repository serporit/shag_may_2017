import java.util.HashMap;

public class Parser {

    public HashMap<String, Integer> parse(String allContent) {

        HashMap<String, Integer> mapOfAllContent = new HashMap<String, Integer>();

        String regularExpression = "[\\n\\r:;,\\(\\)№#\\._'\"\\s\\-\\d]+";
        String[] arrayString = allContent.split(regularExpression);

        for (String temp : arrayString) {

            if (!mapOfAllContent.containsKey(temp)) {
                mapOfAllContent.put(temp, 1);
            } else {
                int tempFrequency = mapOfAllContent.get(temp) + 1;
                mapOfAllContent.put(temp, tempFrequency);
            }
        }
        return mapOfAllContent;
    }
}
