import org.apache.commons.io.FileUtils;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


/**
 * Created by User on 07.06.2017.
 */

public class WordsCount {

    public static void main(String[] args) throws IOException {
        String allFilesContent = "";
        String[] allWords = allFilesContent.split("[\\n\\r:;,\\(\\)№#\\._'\"\\s\\-\\d]+");
        File folder = new File("src\\main\\resources\\books");
        File[] files = folder.listFiles();

        for (File file : files) {
            String content = FileUtils.readFileToString(file);
            allFilesContent = allFilesContent + content;
        }

        allFilesContent = allFilesContent.toLowerCase();

        allWords = allFilesContent.split("[°\\n\\r:;,\\(\\)№#\\._'\"\\s\\-\\d]+");

        Map<String, Integer> wordsAll = new TreeMap<String, Integer>();
        for (String word : allWords) {
            if (wordsAll.containsKey(word)) {
                wordsAll.put(word, wordsAll.get(word) + 1);
            } else {
                if (!"".equals(word)) {wordsAll.put(word, 1);} // добавляем в индекс со значением = 1
            }
        }

        for (Map.Entry<String, Integer> entry : wordsAll.entrySet()) {
            System.out.println("Слово = " + entry.getKey() + ", повторений = " + entry.getValue());
        }
    }
}

