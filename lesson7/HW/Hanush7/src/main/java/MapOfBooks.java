import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by оксанка on 09.06.2017.
 */
public class MapOfBooks {
    public static void main(String[] args) throws Exception {
        File folder = new File("src\\main\\resources\\books");
        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            Map<String, Integer> books = new TreeMap<String, Integer>();
            for (File file : files) {
                String fileContent = FileUtils.readFileToString(file);
                String[] words = fileContent.split("[\\n\\r:;,\\(\\)№#\\._'\"\\s\\-\\d]+");
                for (String word : words) {
                    word = word.toLowerCase();
                    if (books.containsKey(word)) {
                        books.put(word, books.get(word) + 1);
                    } else {
                        if (!"".equals(word)) books.put(word, 1);
                    }
                }

            }
            for (Map.Entry<String, Integer> entry : books.entrySet()) {
                System.out.println("Word  <" + entry.getKey() + "> repeats [" + entry.getValue() + "]");
            }

        } else {
            System.out.println("Enter correct directory");
        }
    }
}



