import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

public class CounterRepeatOfWords {
    public static void main(String[] args) throws IOException {
        String allFilesContent = "";
        File folder = new File("src\\main\\resources\\books");
        File[] files = folder.listFiles();
        for (int i = 0; i < files.length; i++) {
            String content = FileUtils.readFileToString(files[i]);
            allFilesContent = allFilesContent + content;
        }
        String[] words = allFilesContent.split("[\\n\\r:;,\\(\\)№#\\._'\"\\s\\-\\d]+");
        System.out.println("Статистика повторений слов в текстовых файлах");
        System.out.println("из папки src\\main\\resources\\books");
        System.out.println("*********************************************");
        System.out.println("слово - количество повторов");
        System.out.println("*********************************************");

        Map<String, Integer> counterWords = new HashMap<String, Integer>();
        for (String word : words) {
            if (!"".equals(word)) {
                Integer countOfRepeat = counterWords.get(word);
                if (countOfRepeat == null) {
                    countOfRepeat = 0;
                }
                counterWords.put(word, ++countOfRepeat);
            }
        }

        for (String word : counterWords.keySet()) {
            System.out.println(word + " - " + counterWords.get(word));
        }
    }
}


