import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by User on 04.06.2017.
 */
public class Hello {
    public static void main(String[] args) throws IOException {
        moveStudents("src/main/resources/students.txt", 55);
    }


    public static void moveStudents(String filePath, int linesToMove) throws IOException {
        if (linesToMove > 0) {
            File originalFile = new File(filePath);
            if ((originalFile.exists()) && !(originalFile.isDirectory())) {
                List<String> originalFileLines = FileUtils.readLines(originalFile);
                if (linesToMove <= originalFileLines.size()) {
                    ArrayList<String> resultFileLines = new ArrayList<String>();
                    resultFileLines.add(originalFileLines.get(0));
                    for (int i = 0; i < linesToMove; i++) {
                        int randomVar = new Random().nextInt(originalFileLines.size() - 1);
                        int movedLineIndex = randomVar + 1;
                        resultFileLines.add(originalFileLines.get(movedLineIndex));
                        originalFileLines.remove(movedLineIndex);
                    }
                    String newFilePath = generateNewFilePath(filePath);
                    File resultFile = new File(newFilePath);
                    FileUtils.writeLines(resultFile, resultFileLines);
                    FileUtils.writeLines(originalFile, originalFileLines);
                } else
                    System.out.println("Error\n We do not have " + linesToMove + " students. We have " + String.valueOf(originalFileLines.size() - 1));
            } else System.out.println("Error \n File not found OR Path not to file");
        }
    }

    private static String generateNewFilePath(String filePath) {
        int dotIndex = filePath.lastIndexOf(".");
        return (filePath.substring(0, dotIndex) + "_res" + filePath.substring(dotIndex));
    }
}
