package UI;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage {

    @FindBy (tagName = "body")
    private WebElement body;

    @FindBy (partialLinkText = "Район")
    private WebElement aboutThisRegion;

    public MainPage(WebDriver driver, String url) {
        super(driver);
        driver.get(url);
    }

    public boolean checkIsReachable() {
        return body.isDisplayed();
    }

    public void showAbout() {
        aboutThisRegion.click();
    }

}
