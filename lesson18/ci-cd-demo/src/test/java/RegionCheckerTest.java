import UI.MainPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;


import static common.GlobalConfig.getExcelData;

public class RegionCheckerTest {
    private static WebDriver driver;
    private static final Logger log = Logger.getLogger(RegionCheckerTest.class);
    private static final String DATA_PATH = "src/main/resources/data.xls";
    private static final String CHROMEDRIVER_PATH = "src/main/resources/";
    private static String regionsFilter;


    @org.testng.annotations.BeforeClass
    public void precondiitions() {
        log.info("\n\nRegionChecker Test starts");
        regionsFilter= System.getProperty("filter");
        if (regionsFilter==null) regionsFilter="all";
        log.info("Starting preconditions: filter set to "+regionsFilter);
        log.info("Setting system property");
        System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_PATH+"chromedriver");
        log.info("Getting webdriver (chrome local)");
        driver = new ChromeDriver();
        log.info("Maximizing window");
        driver.manage().window().maximize();
    }

    @org.testng.annotations.AfterClass
    public void teardown() {
        log.info("Closing driver");
        driver.quit();
    }

    @org.testng.annotations.Test(description = "Testing if page reachable", dataProvider = "regions")
    public void regionPageReachable(String cat, String url) {
        if (!cat.contains(regionsFilter)&&(regionsFilter!="all")) {throw new SkipException("This url("+cat+") is out of scope - "+ regionsFilter);}
        log.info("Opening "+cat+" page "+url);
        MainPage page = new MainPage(driver,url);
        log.info("Clicking a link");
        page.showAbout();
        Assert.assertTrue(page.checkIsReachable());
    }

    @DataProvider(name="regions")
    public Object[][] getRegions() {
        Object[][] arrayObject = getExcelData(DATA_PATH,"sheet1");
        return arrayObject;
    }








}
