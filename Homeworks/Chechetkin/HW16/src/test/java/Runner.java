import PageObjects.YDPages.YDAllFiles;
import PageObjects.YDPages.YDBusket;
import PageObjects.YDPages.YDDownloads;
import PageObjects.YDPages.YDLoginPage;
import PageObjects.YMPages.ComposePage;
import PageObjects.YMPages.InboxPage;
import PageObjects.YMPages.LoginPage;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import worker.CreatFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by Администратор on 02.07.2017.
 */
public class Runner extends AbstractTest {



    @Test
    public void first() throws IOException {

        LoginPage loginPage = new LoginPage(driver);
        log.info("Create loginPage: \n" + loginPage);
        InboxPage inboxPage = loginPage.enterWithGoodParametrs();

        ComposePage composePage = inboxPage.goToComposePage();
        File file = CreatFile.createFileWithText();
        inboxPage = composePage.sendLetterWithFile(file.getAbsolutePath()).goToInboxPage();
        Assert.assertEquals(file.getName().replaceAll("\\D",""),inboxPage.checkLetter());
        log.info("Latter come back");
        inboxPage.saveFileOnYDisk();
        inboxPage.logOut();

        YDLoginPage ydLoginPage = new YDLoginPage(driver);
        YDAllFiles ydAllFiles = ydLoginPage.enterWithGoodParametrs();
        YDDownloads ydDownloads = ydAllFiles.closeMessage().goToYDDownloads();
        ydAllFiles = ydDownloads.findMoveFile(file.getName()).goToYDAllFiles();
        YDBusket ydBusket = ydAllFiles.dragAndDropToBuscet(file.getName()).goToYDBusket();
        Assert.assertTrue(ydBusket.checkFile(file.getName().replaceAll("\\D","")),"Bad type of Massage");



    }


}
