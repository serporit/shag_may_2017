import Logerator.LoggerObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by Администратор on 14.07.2017.
 */
public class AbstractTest extends LoggerObject{

    protected WebDriver driver;


    @BeforeTest
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }


    @AfterTest
    public void postconditions() throws InterruptedException {
        driver.close();
    }
}
