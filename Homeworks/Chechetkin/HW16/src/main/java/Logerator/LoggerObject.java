package Logerator;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class LoggerObject {

    public static final Logger log = Logger.getLogger(LoggerObject.class);

    public void logGoTo(String namePage, String s) {
        Logger log = Logger.getLogger(s);
        log.info("Go to " + namePage);
    }

    public void logClickButton(String nameButton) {
        log.info("click on button '" + nameButton + "'");
    }

    public void logMessage(String message) {
        log.info(message);
    }


}
