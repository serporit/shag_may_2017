package worker;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import java.io.*;
import java.util.Random;

/**
 * Created by Администратор on 14.07.2017.
 */
public class CreatFile {

    public static File createFileWithText() throws IOException {
        String textForFile = RandomStringUtils.randomAlphanumeric(20);
        String nameForFile = "TestFile_" + RandomStringUtils.randomNumeric(5) + ".txt";
        File file = new File("src\\main\\resources\\"+nameForFile);
        try {
            boolean created = file.createNewFile();
            if (created) System.out.println("File " + nameForFile + " Created");
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
        writer.write(textForFile);
        writer.close();
        return file;
    }
}
