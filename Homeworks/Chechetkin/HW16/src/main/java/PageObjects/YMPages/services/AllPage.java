package PageObjects.YMPages.services;

import PageObjects.YDPages.services.AbstractPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AllPage {
    public static final By MENU_LOCATOR = By.xpath(".//div[@data-key='view=head-user']");
    public static final By EXIT_BUTTON_LOCATOR = By.xpath(".//div[@class='b-mail-dropdown__item'][6]");
    protected WebDriver driver;

    public AllPage(WebDriver driver) {
        this.driver = driver;
    }

    public static final Logger log = Logger.getLogger(AbstractPage.class);

    public void waitForEnementVisible(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void logOut(){
        waitForEnementVisible(MENU_LOCATOR);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Actions actions = new Actions(driver);

        actions.click(driver.findElement(MENU_LOCATOR)).build().perform();
        waitForEnementVisible(EXIT_BUTTON_LOCATOR);
        driver.findElement(EXIT_BUTTON_LOCATOR).click();
    }


    public void logGoTo(String namePage) {
        log.info("Go to " + namePage);
    }

    public void logClickButton(String nameButton) {
        log.info("click on button '" + nameButton + "'");
    }

    public void logMessage(String message) {
        log.info(message);
    }


}
