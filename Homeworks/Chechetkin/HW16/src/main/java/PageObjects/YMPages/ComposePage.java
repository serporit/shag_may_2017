package PageObjects.YMPages;

import PageObjects.YMPages.services.InterfacePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ComposePage extends InterfacePage {

    public static final By FILE_INPUT_LOCATOR = By.xpath(".//div[@class='js-compose-footer-wrapper']//input[@type='file']");

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public ComposePage setAdress(String adress) {
        waitForEnementVisible(By.xpath(".//div[@name='to']"));
        driver.findElement(By.xpath(".//div[@name='to']")).sendKeys(adress);
        logMessage("Write in field adress: " + adress);
        return this;
    }

    public ComposePage setSubj(String subject) {
        driver.findElement(By.xpath(".//input[@name='subj']")).sendKeys(subject);
        logMessage("Write in field subject: " + subject);
        return this;
    }

    public ComposePage waitAndClick(By locator) {
        waitForEnementVisible(locator);
        driver.findElement(locator).click();
        logClickButton(" " + locator);
        return this;
    }

    public ComposePage clickSubmit() {
        waitForEnementVisible(By.xpath("//button[@type='submit']"));
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        logClickButton("submit");
        return this;
    }

    public ComposePage includeFile(String filePath) {
        driver.findElement(FILE_INPUT_LOCATOR).sendKeys(filePath);
        logMessage("Set path : " + filePath);
        return new ComposePage(driver);
    }

    public ComposePage sendLetterWithFile(String filePath) {
        ComposePage composePage = new ComposePage(driver);
        composePage.setAdress("p.a.chechetkin@yandex.by").includeFile(filePath).clickSubmit();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

}
