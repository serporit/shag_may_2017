package PageObjects.YMPages;

import PageObjects.YMPages.services.InterfacePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InboxPage extends InterfacePage {

    public static final By FIRST_THEME_LOCATOR = By.xpath(".//div[@class='mail-MessageSnippet-Content']//span[@class=\"mail-MessageSnippet-Item mail-MessageSnippet-Item_subject\"][1]");
    public static final By FIELD_WITH_FILE_NAME = By.xpath(".//div[@class=\"mail-Message-Attach\"]");


    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public ComposePage goToComposePage(){
        waitForEnementVisible(By.xpath("//a[@href='#compose']//span[@class='mail-ComposeButton-Text']"));
        driver.findElement(By.xpath("//a[@href='#compose']//span[@class='mail-ComposeButton-Text']")).click();
        return new ComposePage(driver);
    }

    public String checkLetter() {
        waitForEnementVisible(FIRST_THEME_LOCATOR);
        driver.findElement(FIRST_THEME_LOCATOR).click();
        waitForEnementVisible(FIELD_WITH_FILE_NAME);
        String numberFromFileName = driver.findElement(FIELD_WITH_FILE_NAME).getText().replaceAll("\\D","");
        return  numberFromFileName;
    }

    public void saveFileOnYDisk(){
        driver.findElement(By.xpath(".//a[@data-hid='1.1']")).click();
    }
}
