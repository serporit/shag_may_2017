package PageObjects.YDPages.services;

import Logerator.LoggerObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractPage extends LoggerObject {
    protected WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }


    protected void waitForEnementVisible(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
        logMessage("Wait element: " + locator);
    }

    protected void waitForEnementVisible(By locator, int time) {
        new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOfElementLocated(locator));
        log.info("Wait element: " + locator + " Time: " + time);
    }


}
