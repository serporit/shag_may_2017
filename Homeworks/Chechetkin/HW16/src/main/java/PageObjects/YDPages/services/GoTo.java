package PageObjects.YDPages.services;

import PageObjects.YDPages.YDAllFiles;
import PageObjects.YDPages.YDBusket;
import PageObjects.YDPages.YDDownloads;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Администратор on 18.07.2017.
 */
public class GoTo extends AbstractPage {

    public static final By DOWNLOADS_LOCATOR = By.xpath(".//div[@class='navigation__item'][4]");
    public static final By LOGO_LOCATOR = By.xpath(".//div[@class='navigation__item'][3]");
    public static final By BUTTON_TRASH_BUSKET_LOCATOR = By.xpath(".//div[@class='navigation__item'][16]");


    public GoTo(WebDriver driver) {
        super(driver);
    }

    public static final Logger log = Logger.getLogger(AbstractPage.class);

    public YDDownloads goToYDDownloads() {
        waitForEnementVisible(DOWNLOADS_LOCATOR);
        driver.findElement(DOWNLOADS_LOCATOR).click();
        logGoTo("Downloads", this.getClass().getSimpleName());
        return new YDDownloads(driver);

    }


    public YDAllFiles goToYDAllFiles() {
        waitForEnementVisible(LOGO_LOCATOR);
        driver.findElement(LOGO_LOCATOR).click();
        logGoTo("All files",this.getClass().getSimpleName());
        return new YDAllFiles(driver);

    }

    public YDBusket goToYDBusket() {
        waitForEnementVisible(BUTTON_TRASH_BUSKET_LOCATOR);
        driver.findElement(BUTTON_TRASH_BUSKET_LOCATOR).click();
        logGoTo("Buscet",this.getClass().getSimpleName());
        return new YDBusket(driver);

    }


}
