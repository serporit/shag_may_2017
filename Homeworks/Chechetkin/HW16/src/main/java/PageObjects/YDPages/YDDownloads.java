package PageObjects.YDPages;

import PageObjects.YDPages.services.GoTo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by Администратор on 18.07.2017.
 */
public class YDDownloads extends GoTo {

    public static final By MOVE_BUTTON_LOCATOR = By.xpath(".//a[@data-click-action='resource.move']");
    public static final By IMAGE_ELSE_BUTTON_LOCATOR = By.xpath(".//img[@class='nb-icon nb-s-three-dots-icon']");
    public static final By BUTTON_MOVE_LOCATOR = By.xpath(".//button[@tabindex='0']");

    public YDDownloads(WebDriver driver) {
        super(driver);
    }

    public YDDownloads findMoveFile(String fileName) {
        waitForEnementVisible(By.xpath(".//div[@data-id='/disk/Загрузки/" + fileName + "']"));
        driver.findElement(By.xpath(".//div[@data-id='/disk/Загрузки/" + fileName + "']")).click();
        logClickButton("with file name " + fileName);
        waitForEnementVisible(IMAGE_ELSE_BUTTON_LOCATOR);
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(IMAGE_ELSE_BUTTON_LOCATOR)).build().perform();
        logClickButton("else");
        waitForEnementVisible(MOVE_BUTTON_LOCATOR);
        actions.click(driver.findElement(MOVE_BUTTON_LOCATOR)).build().perform();
        logClickButton("move");
        waitForEnementVisible(BUTTON_MOVE_LOCATOR);
        actions.click(driver.findElement(BUTTON_MOVE_LOCATOR)).build().perform();
        logClickButton("move to");
        return this;
    }


}
