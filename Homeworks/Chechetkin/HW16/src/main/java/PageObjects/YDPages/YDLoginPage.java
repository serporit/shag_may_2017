package PageObjects.YDPages;

import PageObjects.YDPages.services.GoTo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by Администратор on 14.07.2017.
 */
public class YDLoginPage extends GoTo {

    public static final By BUTTON_SUBMIT_LOCATOR = By.xpath(".//button[@type='submit']");
    public static final By LOGIN_LOCATOR = By.name("login");
    public static final By PASS_LOCATOR = By.name("password");
    public static final String DISK_YANDEX_BY = "https://disk.yandex.by";
    private String login;
    private String password;

    public YDLoginPage(WebDriver driver) {
        super(driver);
    }

    public YDAllFiles enterWithGoodParametrs() {
        YDLoginPage yandexDiskLoginPage = new YDLoginPage(driver);
        return yandexDiskLoginPage.open().enterLogin().enterPass().clickSubmit();
    }

    public YDLoginPage open() {
        driver.get(DISK_YANDEX_BY);
        logMessage("Open site " + DISK_YANDEX_BY);
        return new YDLoginPage(driver);
    }

    public YDLoginPage enterLogin() {
        waitForEnementVisible(LOGIN_LOCATOR);
        Actions actions = new Actions(driver);
        login = "p.a.chechetkin";
        actions.sendKeys(driver.findElement(LOGIN_LOCATOR), login).build().perform();
        logMessage("Enter '" + login + "' in field with locator " + LOGIN_LOCATOR);
        return this;
    }

    public YDLoginPage enterPass() {
        waitForEnementVisible(PASS_LOCATOR);
        Actions actions = new Actions(driver);
        password = "agentbars0007";
        actions.sendKeys(driver.findElement(PASS_LOCATOR), password).build().perform();
        logMessage("Enter '" + password + "' in field with locator " + PASS_LOCATOR);
        return this;

    }

    public YDAllFiles clickSubmit() {
        waitForEnementVisible(BUTTON_SUBMIT_LOCATOR);
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(BUTTON_SUBMIT_LOCATOR)).build().perform();
        logClickButton("submit");
        return new YDAllFiles(driver);
    }
}
