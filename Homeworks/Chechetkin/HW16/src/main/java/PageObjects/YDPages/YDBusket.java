package PageObjects.YDPages;

import PageObjects.YDPages.services.GoTo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by User on 19.07.2017.
 */
public class YDBusket extends GoTo {

    private By FILE_LOCATOR;

    public YDBusket(WebDriver driver) {
        super(driver);
    }

    public boolean checkFile(String fileNumber) {
        try {
            FILE_LOCATOR = By.xpath(".//div[@title='TestFile_" + fileNumber + ".txt']");
            waitForEnementVisible(FILE_LOCATOR,15);
        } catch (Exception e){
            log.error("File not found in busket");
            return false;
        }

        logMessage("File find in buscet");

        return  true;
    }

}
