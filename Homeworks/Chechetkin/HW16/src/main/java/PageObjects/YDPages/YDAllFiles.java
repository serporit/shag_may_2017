package PageObjects.YDPages;

import PageObjects.YDPages.services.GoTo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class YDAllFiles extends GoTo {

    public static final By LOADER_LOCATOR = By.xpath(".//input[@type='file']");
    public static final By CLOSE_MESAGE_LOCATOR = By.xpath(".//a[@class='_nb-popup-close js-dialog-close']");
    public static final By DIALOG_CLOSE_LOCATOR = By.xpath(".//a[@data-click-action='dialog.close']");
    public static final By LOAD_END_LOCATOR = By.xpath(".//div[@class='b-item-upload__icon b-item-upload__icon_done']");
    public static final By BUSCET_LOCATOR = By.xpath(".//div[@class='nb-icon-resource__image nb-icon-resource__image_icon file-icon file-icon_size_m file-icon_dir_trash']");
    private WebElement surceElem;
    private WebElement Buscet;

    public YDAllFiles(WebDriver driver) {
        super(driver);
    }

    public YDAllFiles closeMessage() {
        try {
            waitForEnementVisible(CLOSE_MESAGE_LOCATOR, 2);
            driver.findElement(CLOSE_MESAGE_LOCATOR).click();
            logMessage("window with start message closed");
        } catch (Exception e) {
            logMessage("window with start message don't view");
        }
        return new YDAllFiles(driver);
    }

    public YDAllFiles dragAndDropToBuscet(String fileName){
        Actions actions = new Actions(driver);
        waitForEnementVisible(BUSCET_LOCATOR);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        surceElem = driver.findElement(By.xpath(".//div[@title='" + fileName + "']"));
        Buscet = driver.findElement(BUSCET_LOCATOR);
        actions.dragAndDrop(surceElem, Buscet).build().perform();
        log.info("File traced in buscet");

        return this;
    }

}
