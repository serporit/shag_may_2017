package services;

import org.apache.commons.lang3.RandomStringUtils;
import services.entity.User;
import java.util.Random;

public class RandomUserService {

    private static final int RANDOM_NAME_LENGTH = 10;
    private static final int RANDOM_EMAIL_LENGTH = 4;
    private static final String EMAIL_APPENDIX = "@mailinator.com";
    private static final int RANDOM_PASSWORD_LENGTH = 8;

    public String generateUserName() {
        return new StringBuilder().append("Mr.").append(RandomStringUtils.randomAlphabetic(RANDOM_NAME_LENGTH)).toString();
    }

    public String generateEmailAddress() {
        return new StringBuilder().append(getRandomWeekDay()).append(RandomStringUtils.randomNumeric(RANDOM_EMAIL_LENGTH))
                .append(EMAIL_APPENDIX).toString();
    }

    public String generatePassword() {
        return (RandomStringUtils.randomAlphanumeric(RANDOM_PASSWORD_LENGTH));
    }

    public String getRandomWeekDay() {
        String day;
        switch (new Random().nextInt(6)) {
            case 0:
                day = "monday";
                break;
            case 1:
                day = "tuesday";
                break;
            case 2:
                day = "wednesday";
                break;
            case 3:
                day = "thursday";
                break;
            case 4:
                day = "triday";
                break;
            case 5:
                day = "saturday";
                break;
            case 6:
                day = "sunday";
                break;
            default:
                throw new ArrayIndexOutOfBoundsException();
        }
        return day;
    }

    public User getUser() {
        return new User(generateUserName(), generateEmailAddress(), generatePassword());
    }
}
