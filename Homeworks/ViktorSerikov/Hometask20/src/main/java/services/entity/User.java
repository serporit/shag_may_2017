package services.entity;

public class User {

    private String userName;
    private String userEmailAddress;
    private String userPassword;

    public User(String userName, String userEmailAddress, String userPassword) {
        this.userName = userName;
        this.userEmailAddress = userEmailAddress;
        this.userPassword = userPassword;
    }

    public String getUserEmailAddress() {
        return userEmailAddress;
    }


    public String getUserName() {
        return userName;
    }


    public String getUserPassword() {
        return userPassword;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", userEmailAddress='" + userEmailAddress + '\'' +
                ", userPassword='" + userPassword + '\'' +
                '}';
    }
}
