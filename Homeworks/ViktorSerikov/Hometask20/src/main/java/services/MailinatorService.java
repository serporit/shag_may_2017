package services;

import pages.MailinatorPage;
import services.entity.User;
import static services.LoggerService.getLog;

public class MailinatorService {

    private User user;
    private MailinatorPage mailinatorPage;

    public MailinatorService(MailinatorPage mailinatorPage, User user) {
        this.mailinatorPage = mailinatorPage;
        this.user = user;
    }

    public void confirmSubscribeToListApplePageByEmail() {
        getLog(this).info("Open https://www.mailinator.com");
        mailinatorPage.openMailinatorPage();
        getLog(this).info("Fill Email Address");
        mailinatorPage.fillEmailAddressField(user);
        getLog(this).info("Click to Go button");
        mailinatorPage.clickToGo();
    }

    public void isEmailPresent() {

    }
}
