package services;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

public class Browser {
    public static WebDriver driver;
    public static Browser browser;
    private final int TIME_OF_DENY = 10;

    public Browser() {
    }

    public static Browser current() {
        if (browser == null) {
            browser = new Browser();
        }
        return browser;
    }

    public static WebDriver getCurrentDriver() {
        if (driver == null) {
            startDriver();
        }
        return driver;
    }

    private static void startDriver() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    public void stopDriver() {
        driver.close();
    }

    public Browser click(By locator) {
        new WebDriverWait(driver, TIME_OF_DENY).until(ExpectedConditions.visibilityOfElementLocated(locator));
        driver.findElement(locator).click();
        return this;
    }

    public void writeText(By locator, String text) {
        new WebDriverWait(driver, TIME_OF_DENY).until(ExpectedConditions.visibilityOfElementLocated(locator));
        driver.findElement(locator).sendKeys(text);
    }

    public boolean isElementPresent(By locator){
        return driver.findElement(locator).isDisplayed();
    }

    public void refreshCurrentPage() {
        driver.get(driver.getCurrentUrl());
    }

    public void doubleClick(By locator) {
        new WebDriverWait(driver, TIME_OF_DENY).until(ExpectedConditions.visibilityOfElementLocated(locator));
        new Actions(driver).doubleClick(driver.findElement(locator)).perform();
    }

    public String getText(By locator) {
        new WebDriverWait(driver, TIME_OF_DENY).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator).getText();
    }

    public void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }

    public void waitForVisibilityOfElement(By locator) {
        new WebDriverWait(driver, TIME_OF_DENY).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void initDriver(String url) {
        getCurrentDriver();
    }

    public void openURL(String url) {
        getCurrentDriver().get(url);
    }

    public Set<Cookie> getCookie(){
       return driver.manage().getCookies();
    }


    public void swichToActiveElement() {
        driver.switchTo().defaultContent();

    }
}
