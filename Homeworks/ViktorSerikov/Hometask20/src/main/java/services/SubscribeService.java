package services;

import com.gargoylesoftware.htmlunit.util.Cookie;
import pages.ListsApplePage;
import services.entity.User;

import javax.servlet.http.HttpServletRequest;

import static services.LoggerService.getLog;

public class SubscribeService {

    private User user;
    private ListsApplePage listsApplePage;

    public SubscribeService(ListsApplePage listsApplePage, User user) {
        this.listsApplePage = listsApplePage;
        this.user = user;
    }

    public void subscribeToListApplePage() {
        getLog(this).info("Open  https://lists.apple.com/mailman/listinfo/automator-dev" );
        listsApplePage.openListsApplePage();
        getLog(this).info("Fill email address field");
        listsApplePage.fillEmailAddressField(user);
        getLog(this).info("Fill user name field");
        listsApplePage.fillUserNameField(user);
        getLog(this).info("Fill Password field");
        listsApplePage.fillPickPasswordField(user);
        getLog(this).info("Fill confirm Password fild");
        listsApplePage.fillConfirmPasswordField(user);
        getLog(this).info("Check button Receive List Mail");
        listsApplePage.tickOnToReceiveListMailRadiobutton();
        getLog(this).info("Click to Subscribe");
        listsApplePage.clickToSubscribeForm();
        getLog(this).info("Confirm subscription results");
        listsApplePage.confirmSubscriptionResults();
    }
}
