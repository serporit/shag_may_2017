package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import services.Browser;
import services.entity.User;

import java.util.Set;

public class ListsApplePage {

    private static final String URL = "https://lists.apple.com/mailman/listinfo/automator-dev";
    private static final By CLICK_TO_FILL_EMAIL_ADDRESS_FIELD = By.xpath("//input[@type = 'Text' and @name = 'email']");
    private static final By CLICK_TO_FILL_NAME_FIELD = By.xpath("//input[@type = 'Text' and @name = 'fullname']");
    private static final By CLICK_TO_PICK_PASSWORD_FIELD = By.xpath("//input[@type = 'Password' and @name = 'pw']");
    private static final By CLICK_TO_PICK_PASSWORD_CONFIRM_FIELD = By.xpath("//input[@type = 'Password' and @name = 'pw-conf']");
    private static final By TICK_ON_TO_RECEIVE_MAIL_RBTN = By.xpath("//input[@type = 'radio' and @value= '1']");
    private static final By CLICK_ON_SUBSCRIBE_BTN = By.xpath("//input[@type = 'Submit' and @name = 'email-button']");
    private static final By WAIT_FOR_VISIBLE_AFTER_SUBMIT = By.xpath("//h1[text()['Automator-dev Subscription results']]");
    private static final By CONFIRM_RESULT_AFTER_SUBMIT = By.xpath("//*[text()[contains(.,'Your subscription request has been received')]]");

    public Set<Cookie> getCookie(){
        return Browser.current().getCookie();
    }

    public void openListsApplePage() {
        Browser.current().openURL(URL);
    }

    public void fillEmailAddressField(User user) {
        Browser.current().writeText(CLICK_TO_FILL_EMAIL_ADDRESS_FIELD, user.getUserEmailAddress());
    }

    public void fillUserNameField(User user) {
        Browser.current().writeText(CLICK_TO_FILL_NAME_FIELD, user.getUserName());
    }

    public void fillPickPasswordField(User user) {
        Browser.current().writeText(CLICK_TO_PICK_PASSWORD_FIELD, user.getUserPassword());
    }

    public void fillConfirmPasswordField(User user) {
        Browser.current().writeText(CLICK_TO_PICK_PASSWORD_CONFIRM_FIELD, user.getUserPassword());
    }

    public void tickOnToReceiveListMailRadiobutton() {
        Browser.current().click(TICK_ON_TO_RECEIVE_MAIL_RBTN);
    }

    public void clickToSubscribeForm() {
        Browser.current().click(CLICK_ON_SUBSCRIBE_BTN);
    }

    public void confirmSubscriptionResults() {
        Browser.current().waitForVisibilityOfElement(WAIT_FOR_VISIBLE_AFTER_SUBMIT);
        Browser.current().isElementPresent(CONFIRM_RESULT_AFTER_SUBMIT);
    }
}
