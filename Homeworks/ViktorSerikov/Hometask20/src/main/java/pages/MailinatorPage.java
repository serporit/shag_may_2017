package pages;

import org.openqa.selenium.By;
import services.Browser;
import services.entity.User;

public class MailinatorPage {

    private static final String URL = "https://www.mailinator.com/";
    private static final By CLICK_TO_FILL_EMAIL_ADDRESS_FIELD = By.xpath("//input[@class = 'form-control']");
    private static final By CLICK_ON_SUBSCRIBE_BTN = By.xpath("//button[@class='btn btn-dark']");

    public void openMailinatorPage() {
        Browser.current().openURL(URL);
    }

    public void fillEmailAddressField(User user) {
        Browser.current().writeText(CLICK_TO_FILL_EMAIL_ADDRESS_FIELD, user.getUserEmailAddress());
    }

    public void clickToGo() {
        Browser.current().click(CLICK_ON_SUBSCRIBE_BTN);
    }
}
