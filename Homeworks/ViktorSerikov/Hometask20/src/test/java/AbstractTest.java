import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import pages.ListsApplePage;
import pages.MailinatorPage;
import services.Browser;
import services.MailinatorService;
import services.SubscribeService;
import services.RandomUserService;
import services.entity.User;
import static services.LoggerService.getLog;

public class AbstractTest {

    User user = new RandomUserService().getUser();
    SubscribeService subscribeService = new SubscribeService( new ListsApplePage(),user);
    MailinatorService mailinatorService = new MailinatorService(new MailinatorPage(), user);
    private static String browserProperty;
    private static String randomProperty;

    @BeforeClass
    public void beforeClassPreconditions(){
        browserProperty = System.getProperty("browser");
        randomProperty = System.getProperty("random");
    }

    @BeforeTest
    public void beforeTest() {
        getLog(this).info("Start Preconditions");
        Browser.getCurrentDriver();
    }

    @AfterTest
    public void afterTest() {
        Browser.current().stopDriver();
    }
}
