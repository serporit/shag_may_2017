import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import services.Browser;
import services.DiskService;
import services.MailService;
import services.entity.TestFile;
import services.entity.TestMail;
import services.factory.TestFileFactory;
import services.factory.TestMailFactory;
import static services.LoggerService.getLog;


public class AbstractTest {
    TestFile testFile = TestFileFactory.getTestFile();
    TestMail testMail = TestMailFactory.getTestMail();
    MailService mailService = new MailService();
    DiskService diskService = new DiskService();

    @BeforeClass
    public void startWebDriver(){
        getLog(this).info("Start preconditions");
        getLog(this).info("Start browser");
        Browser.current().getWrappedDriver();
    }

    @AfterClass
    public void closeWebDriver() {
        getLog(this).info("Start afterconditions");
        getLog(this).info("Stop browser");
        Browser.current().stopBrowser();
    }
}
