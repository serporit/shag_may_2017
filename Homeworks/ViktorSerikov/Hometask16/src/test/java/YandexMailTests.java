import org.testng.Assert;
import org.testng.annotations.Test;
import static services.LoggerService.getLog;

public class YandexMailTests extends AbstractTest {

    @Test(description = "verify workflow of sending email with attachment via YandexMail page to YandexDisk and then delete this file")
    public void testSendEmailFromYandexMailPageToYandexDiskAndDelete() {
        //YandexMail
        getLog(this).info("login to \"https://mail.yandex.com\"");
        mailService.logIn();
        getLog(this).info("Create txt file");
        testFile.storeTestFile();
        getLog(this).info("write new EMAIL");
        mailService.writeNewEmail(testMail);
        getLog(this).info("upload a created's txt file");
        mailService.uploadFile(testFile);
        getLog(this).info("submit new Email");
        mailService.submitNewEmail();
        getLog(this).info("open input page");
        mailService.openInputMailPage();
        getLog(this).info("find resent email and download attachment");
        mailService.downloadMailAttachmentFromCurrentMail();
        getLog(this).info("Assert: is Current mail arrived in Inbox");
        Assert.assertTrue(mailService.isCurrentMailArrivedInInbox(testMail), "Mail isn't present in Inbox");
        getLog(this).info("Assert: is Current mail arrived in Out");
        Assert.assertTrue(mailService.isCurrentMailArrivedInOutbox(testMail), "Mail isn't present in Outbox");
        getLog(this).info("logout from \"https://mail.yandex.com\"");
        mailService.logOut();
        // Yandexdisk
        getLog(this).info("login to https://disk.yandex.ru/");
        diskService.loginToYandexDisk();
        getLog(this).info("Open downloaded page");
        diskService.openDownloadsPage();
        getLog(this).info("Find downloaded file");
        diskService.findFile(testFile);
        getLog(this).info("move file to root folder");
        diskService.moveFileToRootFolder();
        getLog(this).info("Open allFiles page");
        diskService.openAllFiles();
        getLog(this).info("Delete file which was moved");
        diskService.moveToTrash(testFile);
        getLog(this).info("Assert: deleted file places in trash bin");
        Assert.assertTrue(diskService.checkDeletedFile(testFile), "File doesn't exist in the trash bin");
        getLog(this).info("logout from https://disk.yandex.ru/");
        diskService.logOutFromYandexDisk();
    }
}
