package services.entity;

public class TestMail {

    private String testMailArticle;
    private String testMailAdress;
    private String testMailContent;

    public TestMail(String mailArticle, String testMailAdress, String testMailContent) {
        this.testMailArticle = mailArticle;
        this.testMailAdress = testMailAdress;
        this.testMailContent = testMailContent;
    }

    public String getTestMailArticle() {
        return testMailArticle;
    }

    public void setTestMailArticle(String testMailArticle) {
        this.testMailArticle = testMailArticle;
    }

    public String getTestMailAdress() {
        return testMailAdress;
    }

    public void setTestMailAdress(String testMailAdress) {
        this.testMailAdress = testMailAdress;
    }

    public String getTestMailContent() {
        return testMailContent;
    }

    public void setTestMailContent(String testMailContent) {
        this.testMailContent = testMailContent;
    }
}
