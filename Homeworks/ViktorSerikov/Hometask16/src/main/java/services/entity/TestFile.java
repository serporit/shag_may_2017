package services.entity;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TestFile {

    private String testFileName;
    private String testFilePath;
    private String testFileContent;

    public TestFile(String fileName, String filePath, String content) {
        this.testFileName = fileName;
        this.testFileContent = content;
        this.testFilePath = "";
    }

    public String getTestFileName() {
        return testFileName;
    }

    public String getTestFilePath() {
        return testFilePath;
    }

    public void setTestFilePath(String path) {
        this.testFilePath = path;
    }

    public String getTestFileContent() {
        return testFileContent;
    }

    public void storeTestFile() {
        File file = new File(this.getTestFileName());
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            try {
                out.print(this.getTestFileContent());
                this.setTestFilePath(file.getAbsolutePath());
            } finally {
                out.close();
            }
        } catch (IOException e) {
            System.out.println("Write file error!");
        }
    }
}
