package services;

import screen.yandexMail.*;
import services.entity.TestFile;
import services.entity.TestMail;
import static services.entity.TestYandexUser.YLogin;
import static services.entity.TestYandexUser.YPassword;
import static services.LoggerService.getLog;

public class MailService {

    YandexMailMainPage yandexMailMainPage = new YandexMailMainPage();
    YandexMailWriteEmailPage yandexMailWriteEmailPage = new YandexMailWriteEmailPage();
    YandexMailInboxPage yandexMailInboxPage = new YandexMailInboxPage();
    YandexMailOutputPage yandexMailOutputPage = new YandexMailOutputPage();
    YandexMailLoginPage yandexMailLoginPage = new YandexMailLoginPage();

    public void writeNewEmail(TestMail testMail) {
        getLog(this).info("Click to open page to write new Email");
        yandexMailMainPage.clickToWriteNewEmail();
        getLog(this).info("Fill address data");
        yandexMailWriteEmailPage.fillAddress(testMail);
        getLog(this).info("Fill article data");
        yandexMailWriteEmailPage.fillArticle(testMail);
    }

    public void submitNewEmail() {
        getLog(this).info("Click to send Email");
        yandexMailWriteEmailPage.clickToSendEmail();
    }

    public void downloadMailAttachmentFromCurrentMail() {
        getLog(this).info("Click to mail attachment");
        yandexMailInboxPage.clickToDownloadFileToDisk();
    }

    public boolean isCurrentMailArrivedInInbox(TestMail testMail) {
        yandexMailMainPage.refreshCurrentPage();
        getLog(this).info("Open Inbox page");
        yandexMailMainPage.clickToOpenInputBox();
        getLog(this).info("Find the same File in Inbox page");
        return yandexMailInboxPage.findTheSameTitleOfLastMailInInbox(testMail);
    }

    public boolean isCurrentMailArrivedInOutbox(TestMail testMail) {

        yandexMailMainPage.refreshCurrentPage();
        getLog(this).info("Open Outbox page");
        yandexMailMainPage.clickToOpenOutputBox();
        getLog(this).info("Find the same File in Outbox page");
        return yandexMailOutputPage.findTheSameTitleOfLastMailInOutbox(testMail);
    }

    public void logOut() {
        yandexMailMainPage.pause();
        getLog(this).info("Open user setiings");
        yandexMailMainPage.clickToOpenUserSettings();
        yandexMailMainPage.pause();
        getLog(this).info("Click to log out user");
        yandexMailMainPage.clickToLogOutUser();
    }

    public void uploadFile(TestFile testFile) {
        getLog(this).info("Upload File");
        yandexMailWriteEmailPage.uploadFile(testFile);
        Browser.current().pause();
    }

    public void openInputMailPage() {
        getLog(this).info("Click to open Mail page");
        yandexMailMainPage.clickToOpenInputBox();
        Browser.current().pause();
    }

    public void logIn() {
        getLog(this).info("Open page");
        yandexMailLoginPage.open();
        getLog(this).info("Fill Login and Password");
        yandexMailLoginPage.login(YLogin, YPassword);
    }
}
