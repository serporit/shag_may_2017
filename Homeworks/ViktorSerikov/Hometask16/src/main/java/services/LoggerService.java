package services;

import org.apache.log4j.Logger;

public class LoggerService {
    public static Logger getLog(Object nameClass){
         return Logger.getLogger(nameClass.getClass());
    }
}
