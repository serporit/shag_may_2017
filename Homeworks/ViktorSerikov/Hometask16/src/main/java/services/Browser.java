package services;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Browser {

    public final static int COMMON_ELEMENT_WAIT_TIME_OUT = 15;
    private static Browser browser;
    private static WebDriver driver;

    private Browser() {
    }

    public static Browser current() {
        if (browser == null) {
            browser = new Browser();
        }
        return browser;
    }

    private static void startBrowser() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    public static void stopBrowser() {
        driver.close();
    }

    public WebDriver getWrappedDriver() {
        if (driver == null) startBrowser();
        return driver;
    }

    public void waitForElementIsClickable(By locator) {
        try {
            new WebDriverWait(driver, COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.elementToBeClickable(locator));
        } catch (TimeoutException e) {
            pause();
        }
    }

    public void waitForElementIsDisplayed(By locator) {
        try {
            new WebDriverWait(driver, COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            pause();
        }
    }

    public boolean isPresent(By locator) {
        return driver.findElement(locator).isDisplayed();
    }

    public void waitForDisappear(By locator) {
        try {
            new WebDriverWait(driver, COMMON_ELEMENT_WAIT_TIME_OUT)
                    .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            pause();
        }
    }

    public void waitForAppear(By locator) {
        new WebDriverWait(driver, COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void writeText(By locator, String text) {
        driver.findElement(locator).sendKeys(text);
    }

    public void refreshCurrentPage() {
        driver.get(driver.getCurrentUrl());
    }

    public void writeTextByAction(By locator, String text) {
        new Actions(driver).sendKeys(driver.findElement(locator), text).perform();
    }

    public void click(By locator) {
        new Actions(driver).click(driver.findElement(locator)).perform();
    }

    public void moveToElement(By locatorHover, By locatorClick) {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(locatorHover)).click(driver.findElement(locatorClick));
        Action mouseOverAndClick = actions.build();
        mouseOverAndClick.perform();
    }

    public void doubleClick(By locator) {
        new Actions(driver).doubleClick(driver.findElement(locator)).perform();
    }

    public String getText(By locator) {
        return driver.findElement(locator).getText();
    }

    public void clear(By locator) {
        driver.findElement(locator).clear();
    }

    public void clearInputField(By locator) {
        WebElement toClear = driver.findElement(locator);
        toClear.sendKeys(Keys.CONTROL + "a");
        toClear.sendKeys(Keys.DELETE);
    }

    public void submit(By locator) {
        driver.findElement(locator).submit();
    }

    public void dragTo(By draggable, By target) {
        Actions action = new Actions(driver);
        action.dragAndDrop(driver.findElement(draggable), driver.findElement(target)).build().perform();
    }

    public void clickWithCTRL(By locator) {
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL).click(driver.findElement(locator)).build().perform();
    }

    public void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }

    public void openURL(String url) {
        getWrappedDriver().get(url);
    }
}
