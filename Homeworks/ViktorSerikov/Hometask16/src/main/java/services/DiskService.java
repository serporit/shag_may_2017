package services;

import screen.yandexDisk.YandexDiskAllFilesPage;
import screen.yandexDisk.YandexDiskDownloadsPage;
import screen.yandexDisk.YandexDiskLoginPage;
import screen.yandexDisk.YandexDiskMainPage;
import services.entity.TestFile;

import static services.LoggerService.getLog;
import static services.entity.TestYandexUser.YLogin;
import static services.entity.TestYandexUser.YPassword;

public class DiskService {
    YandexDiskMainPage yandexDiskMainPage = new YandexDiskMainPage();
    YandexDiskAllFilesPage yandexDiskAllFilesPage = new YandexDiskAllFilesPage();
    YandexDiskDownloadsPage yandexDiskDownloadsPage = new YandexDiskDownloadsPage();
    YandexDiskLoginPage yandexDiskLoginPage = new YandexDiskLoginPage();

    public void openDownloadsPage() {
        yandexDiskMainPage.openDownloadsPage();
    }

    public void logOutFromYandexDisk() {
        getLog(this).info("Click to open User settings");
        yandexDiskMainPage.clickToOpenUserSettings();
        getLog(this).info("Click to log out user");
        yandexDiskMainPage.clickToLogOutUser();
    }

    public void findFile(TestFile testFile) {
        getLog(this).info("Find file by name");
        yandexDiskDownloadsPage.findFileByName(testFile);
    }

    public void moveFileToRootFolder() {
        getLog(this).info("Click to open File settings ");
        yandexDiskDownloadsPage.clickToOpenFileSettingsMore();
        getLog(this).info("Click to open move File");
        yandexDiskDownloadsPage.clickToOpenMoveMenu();
        getLog(this).info("Click to submit movement File to Root folder");
        yandexDiskDownloadsPage.clickToSubmitMoveMenu();
    }

    public void openAllFiles() {
        getLog(this).info("Click to open All downloads page");
        yandexDiskMainPage.openAllDownloadsFilePage();
    }

    public void moveToTrash(TestFile testFile) {
        getLog(this).info("Move to trash bin File");
        yandexDiskAllFilesPage.moveToTrash(testFile);
    }

    public boolean checkDeletedFile(TestFile testFile) {
        getLog(this).info("Click to open Trash bin");
        yandexDiskAllFilesPage.ClickToTrashBin();
        getLog(this).info("Make sure that current File is plased in Trash bin");
        return yandexDiskAllFilesPage.isDeletedFilePresent(testFile);
    }

    public void loginToYandexDisk() {
        getLog(this).info("Open yandex disk page");
        yandexDiskLoginPage.open();
        getLog(this).info("Fill Login and Password");
        yandexDiskLoginPage.login(YLogin, YPassword);
    }
}
