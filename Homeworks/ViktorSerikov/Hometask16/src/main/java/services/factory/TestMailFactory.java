package services.factory;

import org.apache.commons.lang3.RandomStringUtils;
import services.entity.TestMail;
import services.entity.TestYandexUser;

public class TestMailFactory {

    public final static int TEST_ARTICLE_LENGTH = 8;
    public final static int MAIL_CONTENT_LENGTH = 3000;

    public static TestMail getTestMail() {
        String testMailArticle = "Test " + RandomStringUtils.randomAlphanumeric(TEST_ARTICLE_LENGTH);
        String testMailAdress = TestYandexUser.YLogin;
        String testMailContent = "mail content " + RandomStringUtils.randomAlphanumeric(MAIL_CONTENT_LENGTH);
        return new TestMail(testMailArticle, testMailAdress, testMailContent);
    }

    //overload
    public static TestMail getTestMail(String address) {
        String testMailArticle = "Test " + RandomStringUtils.randomAlphanumeric(TEST_ARTICLE_LENGTH);
        String testMailAdress = address;
        String testMailContent = "mail content " + RandomStringUtils.randomAlphanumeric(MAIL_CONTENT_LENGTH);
        return new TestMail(testMailArticle, testMailAdress, testMailContent);
    }


    //overload
    public static TestMail getTestMail(String address, String article) {
        String testMailArticle = article;
        String testMailAdress = address;
        String testMailContent = "mail content " + RandomStringUtils.randomAlphanumeric(MAIL_CONTENT_LENGTH);
        return new TestMail(testMailArticle, testMailAdress, testMailContent);
    }
}
