package screen;

import services.Browser;

public class AbstractYandexPage {

    public void pause() {
        Browser.current().pause();
    }

    public void refreshCurrentPage() {
        Browser.current().refreshCurrentPage();
    }
}
