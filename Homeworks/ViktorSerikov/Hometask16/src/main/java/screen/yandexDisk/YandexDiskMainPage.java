package screen.yandexDisk;

import org.openqa.selenium.By;
import screen.AbstractYandexPage;
import services.Browser;

public class YandexDiskMainPage extends AbstractYandexPage {

    public static final By CLICK_TO_OPEN_ATTACHMENTS_PAGE = By.xpath("//div[@class='navigation']/div/div[7]/a");
    public static final By CLICK_TO_OPEN_DOWNLOADS_PAGE = By.xpath("//div[@class='navigation']/div/div[4]/a");
    public static final By CLICK_TO_OPEN_ALL_DOWNLOADS_PAGE = By.xpath("//div[@class='navigation']/div/div[3]/a");
    private static final By CLICK_TO_OPEN_USER_SETTINGS_LOCATOR = By.xpath("//*[@class ='header__username']");
    private static final By CLICK_TO_CLICK_EXIT_LOCATOR = By.xpath("//ul[@class='menu menu_size_s menu_theme_normal menu_type_navigation header__user-menu']//li[2]//span");

    public void openAttachmentsPage() {
        Browser.current().waitForElementIsClickable(CLICK_TO_OPEN_ATTACHMENTS_PAGE);
        Browser.current().click(CLICK_TO_OPEN_ATTACHMENTS_PAGE);
    }

    public void openDownloadsPage() {
        Browser.current().waitForElementIsClickable(CLICK_TO_OPEN_DOWNLOADS_PAGE);
        Browser.current().click(CLICK_TO_OPEN_DOWNLOADS_PAGE);
    }

    public void clickToOpenUserSettings() {
        Browser.current().waitForElementIsDisplayed(CLICK_TO_OPEN_USER_SETTINGS_LOCATOR);
        Browser.current().click(CLICK_TO_OPEN_USER_SETTINGS_LOCATOR);
    }

    public void clickToLogOutUser() {
        Browser.current().waitForElementIsDisplayed(CLICK_TO_CLICK_EXIT_LOCATOR);
        Browser.current().click(CLICK_TO_CLICK_EXIT_LOCATOR);
    }

    public void openAllDownloadsFilePage() {
        Browser.current().waitForElementIsClickable(CLICK_TO_OPEN_ALL_DOWNLOADS_PAGE);
        Browser.current().click(CLICK_TO_OPEN_ALL_DOWNLOADS_PAGE);
    }
}
