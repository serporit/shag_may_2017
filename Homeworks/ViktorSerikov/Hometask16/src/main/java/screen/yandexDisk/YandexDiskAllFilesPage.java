package screen.yandexDisk;

import org.openqa.selenium.By;
import services.Browser;
import services.entity.TestFile;

public class YandexDiskAllFilesPage extends YandexDiskMainPage {

    public static final By TRASH_BIN_LOCATOR = By.xpath(".//*[@class = 'b-content']//*[@title='Корзина']");

    public void moveToTrash(TestFile testFile) {
        final By TEMP_LOCATOR = By.xpath(".//div[@class='recent-files__content']//div[@title='" + testFile.getTestFileName() + "']");
        Browser.current().waitForElementIsDisplayed(TEMP_LOCATOR);
        Browser.current().dragTo(TEMP_LOCATOR, TRASH_BIN_LOCATOR);
        Browser.current().waitForDisappear(TEMP_LOCATOR);
    }

    public boolean isDeletedFilePresent(TestFile testFile) {
        By TEMP_LOCATOR = By.xpath(".//*[@title='" + testFile.getTestFileName() + "']");
        Browser.current().waitForElementIsDisplayed(TEMP_LOCATOR);
        return Browser.current().isPresent(TEMP_LOCATOR);
    }

    public void ClickToTrashBin() {
        Browser.current().waitForElementIsClickable(TRASH_BIN_LOCATOR);
        Browser.current().doubleClick(TRASH_BIN_LOCATOR);

    }
}
