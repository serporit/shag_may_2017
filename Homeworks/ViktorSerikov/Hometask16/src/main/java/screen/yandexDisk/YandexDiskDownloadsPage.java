package screen.yandexDisk;

import org.openqa.selenium.By;
import services.Browser;
import services.entity.TestFile;

public class YandexDiskDownloadsPage extends YandexDiskMainPage {

    public static final By DATA_CLICK_ACTION_RESOURCE_MOVE = By.xpath(".//*[@data-click-action='resource.move']");
    private static final By CLICK_TO_OPEN_SETTINGS_FILE_MORE = By.xpath("//*[@class=' nb-button _nb-small-button _nb-with-only-button _init js-popup-toggler']");
    private static final By CLICK_TO_SUBMIT_MOVE_MENU = By.xpath(".//*[@class = 'nb-button _nb-small-action-button _init js-folders-accept b-dialog" +
            "__button_right ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']");

    public void findFileByName(TestFile testFile) {
        final By TEMP_LOCATOR = By.xpath(".//div[@class='ns-view-container-desc']/div[contains(*," + testFile.getTestFileName() + ")]");
        Browser.current().waitForElementIsDisplayed(TEMP_LOCATOR);
        Browser.current().click(TEMP_LOCATOR);
    }

    public void clickToOpenFileSettingsMore() {
        Browser.current().waitForElementIsClickable(CLICK_TO_OPEN_SETTINGS_FILE_MORE);
        Browser.current().click(CLICK_TO_OPEN_SETTINGS_FILE_MORE);
    }

    public void clickToOpenMoveMenu() {
        Browser.current().waitForElementIsClickable(DATA_CLICK_ACTION_RESOURCE_MOVE);
        Browser.current().click(DATA_CLICK_ACTION_RESOURCE_MOVE);
    }

    public void clickToSubmitMoveMenu() {
        Browser.current().waitForElementIsClickable(CLICK_TO_SUBMIT_MOVE_MENU);
        Browser.current().click(CLICK_TO_SUBMIT_MOVE_MENU);
    }
}
