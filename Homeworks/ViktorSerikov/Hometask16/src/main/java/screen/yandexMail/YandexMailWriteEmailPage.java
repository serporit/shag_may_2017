package screen.yandexMail;

import org.openqa.selenium.By;
import services.Browser;
import services.entity.TestFile;
import services.entity.TestMail;

public class YandexMailWriteEmailPage extends YandexMailMainPage {

    private static final By ADRESS_INPUT_LOCATOR = By.xpath(".//div[@name='to']");
    private static final By ARTICLE_INPUT_LOCATOR = By.xpath(".//*[@name='subj']");
    private static final By WAIT_LOCATOR_BEFORE_LEAVE_NEX_MAIBOX = By.xpath("//*[@class=\"mail-Done-Title js-title-info\"]");
    private static final By CLICK_TO_ATTACH_LOCATOR = By.xpath(".//*[@class = 'mail-Compose-Field-Actions-Footer-Main'] //*[@name = 'att']");
    private static final By SEND_MAIL_LOCATOR = By.xpath("//div[@class='mail-Compose-Field-Actions_left' ]//button");

    public void clickToSendEmail() {
        Browser.current().waitForElementIsDisplayed(SEND_MAIL_LOCATOR);
        Browser.current().click(SEND_MAIL_LOCATOR);
        Browser.current().waitForElementIsDisplayed(WAIT_LOCATOR_BEFORE_LEAVE_NEX_MAIBOX);
    }

    public void fillAddress(TestMail testMail) {
        Browser.current().waitForElementIsDisplayed(ADRESS_INPUT_LOCATOR);
        Browser.current().writeText(ADRESS_INPUT_LOCATOR, testMail.getTestMailAdress());
    }

    public void fillArticle(TestMail testMail) {
        Browser.current().waitForElementIsDisplayed(ARTICLE_INPUT_LOCATOR);
        Browser.current().click(ARTICLE_INPUT_LOCATOR);
        Browser.current().writeText(ARTICLE_INPUT_LOCATOR, testMail.getTestMailArticle());
    }

    public void uploadFile(TestFile testFile) {
        Browser.current().waitForElementIsDisplayed(CLICK_TO_ATTACH_LOCATOR);
        Browser.current().writeText(CLICK_TO_ATTACH_LOCATOR, testFile.getTestFilePath());
    }
}
