package screen.yandexMail;

import org.openqa.selenium.By;
import screen.AbstractYandexPage;
import services.Browser;

public class YandexMailMainPage extends AbstractYandexPage {
    private static final By USER_LOGIN_LOCATOR = By.className("mail-User-Name");
    private static final By WRITE_NEW_EMAIL_BUTTON_LOCATOR = By.className("mail-ComposeButton");
    private static final By CLICK_TO_OPEN_USER_SETTINGS_LOCATOR = By.xpath("//*[@class = 'mail-User-Picture js-user-picture']");
    private static final By CLICK_TO_LOGOUT_LOCATOR_IN_USER_SETTINGS = By.xpath(".//div[@class ='b-user-dropdown-content b-user-dropdown-content-with-exit']/div[9]");
    private static final By CLICK_TO_INPUT_MAILBOX_LOCATOR = By.xpath(".//a[@data-key=\"view=folder&fid=1\"]");
    private static final By CLICK_TO_OUTPUT_MAILBOX_LOCATOR = By.xpath(".//a[@data-fid=\"4\"]//span[@class=\"mail-NestedList-Item-Name js-folders-item-name\"]");

    public String getLogin() {
        Browser.current().waitForElementIsDisplayed(USER_LOGIN_LOCATOR);
        return Browser.current().getText(USER_LOGIN_LOCATOR);
    }

    public void clickToWriteNewEmail() {
        Browser.current().waitForElementIsDisplayed(WRITE_NEW_EMAIL_BUTTON_LOCATOR);
        Browser.current().click(WRITE_NEW_EMAIL_BUTTON_LOCATOR);
    }

    public void clickToOpenUserSettings() {
        Browser.current().waitForElementIsClickable(CLICK_TO_OPEN_USER_SETTINGS_LOCATOR);
        Browser.current().click(CLICK_TO_OPEN_USER_SETTINGS_LOCATOR);
    }

    public void clickToLogOutUser() {
        Browser.current().waitForElementIsDisplayed(CLICK_TO_LOGOUT_LOCATOR_IN_USER_SETTINGS);
        Browser.current().click(CLICK_TO_LOGOUT_LOCATOR_IN_USER_SETTINGS);
    }

    public void clickToOpenInputBox() {
        Browser.current().waitForElementIsDisplayed(CLICK_TO_INPUT_MAILBOX_LOCATOR);
        Browser.current().click(CLICK_TO_INPUT_MAILBOX_LOCATOR);
    }

    public void clickToOpenOutputBox() {
        Browser.current().waitForElementIsDisplayed(CLICK_TO_OUTPUT_MAILBOX_LOCATOR);
        Browser.current().click(CLICK_TO_OUTPUT_MAILBOX_LOCATOR);
    }
}
