package screen.yandexMail;

import org.openqa.selenium.By;
import services.Browser;
import services.entity.TestMail;

public class YandexMailOutputPage extends YandexMailMainPage {

    public boolean findTheSameTitleOfLastMailInOutbox(TestMail testMail) {
        final By FIND_THE_SAME_TITLE_IN_MAILBOX = By.xpath(".//*[@title ='" + testMail.getTestMailArticle() + "']");
        Browser.current().waitForElementIsDisplayed(FIND_THE_SAME_TITLE_IN_MAILBOX);
        return Browser.current().isPresent(FIND_THE_SAME_TITLE_IN_MAILBOX);
    }
}
