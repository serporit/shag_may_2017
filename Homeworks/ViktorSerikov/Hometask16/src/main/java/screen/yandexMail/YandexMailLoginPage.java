package screen.yandexMail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.Browser;
import static services.LoggerService.getLog;

public class YandexMailLoginPage {

    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
    private static final By ENTER_BUTTON_LOCATOR = By.cssSelector("span.new-auth-submit button");
    private static final String BASE_URL = "https://mail.yandex.com";

    public void open() {
        getLog(this).info("Open" + BASE_URL);
        Browser.current().openURL(BASE_URL);
    }

    public void login(String login, String password) {
        fillLoginInput(login);
        fillPasswordInput(password);
        clickEnter();
    }

    public void fillLoginInput(String login) {
        Browser.current().waitForElementIsDisplayed(LOGIN_INPUT_LOCATOR);
        Browser.current().clear(LOGIN_INPUT_LOCATOR);
        Browser.current().writeText(LOGIN_INPUT_LOCATOR, login);
    }

    public void fillPasswordInput(String password) {

        Browser.current().waitForElementIsDisplayed(PASSWORD_INPUT_LOCATOR);
        Browser.current().writeText(PASSWORD_INPUT_LOCATOR, password);
    }

    public void clickEnter() {
        Browser.current().waitForElementIsDisplayed(ENTER_BUTTON_LOCATOR);
        Browser.current().click(ENTER_BUTTON_LOCATOR);
    }
}
