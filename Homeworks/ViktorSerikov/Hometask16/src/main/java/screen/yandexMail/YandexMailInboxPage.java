package screen.yandexMail;

import org.openqa.selenium.By;
import services.Browser;
import services.entity.TestMail;

public class YandexMailInboxPage extends YandexMailMainPage {

    private static final By HOVER_TO_OPEN_DOWNLOAD_MENU =
            By.xpath("//div[@class='ns-view-container-desc mail-MessagesList js-messages-list']/div[1]" +
                    "//a[@class='mail-File-Actions-Item js-skip-click-message-item js-attachment-actions-item mail-File-Actions-Item_main js-preview-attachment']");
    private static final By CLICK_TO_DOWNLOAD_ATTACHMENT_LOCATOR =
            By.xpath("//div[@class='ns-view-container-desc mail-MessagesList js-messages-list']/div[1]" +
                    "//a[@data-hid=\"1.1\"]");

    public void clickToDownloadFileToDisk() {
        Browser.current().refreshCurrentPage();
        Browser.current().waitForElementIsClickable(HOVER_TO_OPEN_DOWNLOAD_MENU);
        Browser.current().moveToElement(HOVER_TO_OPEN_DOWNLOAD_MENU, CLICK_TO_DOWNLOAD_ATTACHMENT_LOCATOR);
        Browser.current().pause();
    }

    public boolean findTheSameTitleOfLastMailInInbox(TestMail testMail) {
        final By FIND_THE_SAME_TITLE_IN_MAILBOX = By.xpath(".//*[@title ='" + testMail.getTestMailArticle() + "']");
        Browser.current().waitForElementIsDisplayed(FIND_THE_SAME_TITLE_IN_MAILBOX);
        return Browser.current().isPresent(FIND_THE_SAME_TITLE_IN_MAILBOX);
    }
}
