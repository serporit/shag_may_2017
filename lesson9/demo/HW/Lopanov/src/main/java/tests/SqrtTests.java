package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SqrtTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Sqrt test", dataProvider = "testData")
    public void sqrtTestDemo(double num1) {
        double actual = calculator.sqrt(num1);
        double expected = Math.sqrt(num1);
        Assert.assertEquals(actual, expected, "\n" +
                "Attention!!!");
    }
    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {1},
                {-4},
                {4},
                {0},
                {-1.4e-45f},
                {3.4e+38f},
                {-4.9e-324},
                {1.7e+308},
        };
    }
}
