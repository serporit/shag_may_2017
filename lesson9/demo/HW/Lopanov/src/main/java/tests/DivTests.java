package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DivTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Div test", dataProvider = "testData")
    public void divTestDemo(double num1, double num2) {
        double actual = calculator.div(num1, num2);
        double expected = num1 / num2;
        Assert.assertEquals(actual, expected, "\n" +
                "Attention!!!");
    }
    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {4, 2},
                {-4, 5},
                {4, 0},
                {-4, 0},
                {0, 0},
                {0, 5}
        };
    }
}
