package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SqrTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Sqr test", dataProvider = "testData")
    public void multTestDemo(double num1) {
        double actual = calculator.sqr(num1);
        double expected = Math.pow(num1, Math.abs(2));
        Assert.assertEquals(actual, expected, "\n" +
                "Attention!!!");
    }
    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {1},
                {5},
                {-1},
                {-4},
                {0},
                {-1.4e-45f},
                {3.4e+38f},
                {-4.9e-324},
                {1.7e+308},
        };
    }
}
