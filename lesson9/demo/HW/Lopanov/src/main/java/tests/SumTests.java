package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SumTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Sum test", dataProvider = "testData")
    public void sumTestDemo(double num1, double num2) {
        double actual = calculator.sum(num1, num2);
        double expected = num1 + num2;
        Assert.assertEquals(actual, expected, "ALARM!!!");}

    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {4, 5},
                {-4, 5},
                {4, -5},
                {-4, -5},
                {-1.4e-45f, -1},
                {3.4e+38f, 1},
                {-4.9e-324, -1},
                {1.7e+308, 1},
        };
    }
}
