package runner;

import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList();
        suites.add("src\\main\\resources\\suites\\testng.xml");
        testNG.addListener(new CustomTestListener());
        testNG.setTestSuites(suites);
        testNG.run();
    }
}
