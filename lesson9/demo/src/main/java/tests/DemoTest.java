package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DemoTest {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Summ test", dataProvider = "testData")
    public void sumTestDemo(double num1, double num2) {
        double actual = calculator.sum(num1, num2);
        double expected = num1 + num2;
        Assert.assertEquals(actual, expected, "ALARM!!!");
    }

    @Test(description = "Div test", expectedExceptions = ArithmeticException.class)
    public void testDemoWithException() {
        int i = 1/0;
    }

    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {4, 5},
                {-4, 5},
                {4, -5},
                {-4, -5}
        };
    }
}
