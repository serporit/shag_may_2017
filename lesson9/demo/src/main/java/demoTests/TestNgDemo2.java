package demoTests;

import bo.Quadro;
import org.testng.Assert;
import org.testng.annotations.*;

public class TestNgDemo2 {
    Quadro quadro;

//    @BeforeMethod // выполнится перед каждым тестовым методом
    @BeforeClass  // выполнится один раз перед тестовымиметодами в текущем классе
    public void init(){
        quadro = new Quadro();
        System.out.println("Created new Quadro instance");
    }

    @Test(description = "check how square() method works")
    @Parameters
    public void testSomeClass() {
        int input = 7;
        int result = quadro.square(input);
        int expected = 49;
        Assert.assertEquals(result, expected, "Incorect work of square method");
    }

    @Test(description = "check how square() method works")
    public void testSomeClass1() {
        int input = 7;
        int result = quadro.square(input);
        int expected = 49;
        Assert.assertEquals(result, expected, "Incorect work of square method");
    }

    @Test(description = "check how square() method works")
    public void testSomeClass2() {
        int input = 7;
        int result = quadro.square(input);
        int expected = 48;
        Assert.assertEquals(result, expected, "Incorect work of square method");
    }

    @Test(description = "check how square() method works")
    public void testSomeClass3() {
        int input = 7;
        int result = quadro.square(input);
        int expected = 49;
        Assert.assertEquals(result, expected, "Incorect work of square method");
    }

    @Test
    public void testIntVsDouble() {
        int num1 = 49;
        double num2 = 49;
        Assert.assertEquals(num1, num2);
    }

    @Test
    public void testIntVsDouble2() {
        Integer num1 = 49;
        Double num2 = 49.0;
        Assert.assertTrue(num1.equals(num2));
    }
}
