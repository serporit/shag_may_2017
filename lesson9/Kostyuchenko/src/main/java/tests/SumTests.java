package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SumTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Summ test double", dataProvider = "testDataNum")
    public void sumTestDouble(double num1, double num2) {
        double actual = calculator.sum(num1, num2);
        double expected = num1 + num2;
        Assert.assertEquals(actual, expected, "Error for "+ num1 + "+" + num2 +"!!!");
    }

    @Test(description = "Summ test char", dataProvider = "testDataChar")
    public void sumTestChar(double num1, double num2) {
        double actual = calculator.sum(num1, num2);
        double expected = num1 + num2;
        Assert.assertEquals(actual, expected, String.format("Error for %s+%s!!!", num1, num2));
    }


    @DataProvider
    public Object[][] testDataNum() {
        return new Object[][]{
                {20, 53},
                {-4, 100000000},
                {-2147483648, 2147483647},
                {-2147483648, -1},
                {-2147483648, 0},
                {-2147483648, 1},
                {2147483647, 1},
                {2147483647, 0},
                {2147483647, -1},
                {0.01, 1.00001400000000000000000010101040000000000000010},
                {0, 0},
                {-1, 0},
                {0, 1},
                {0.000000000000, -1.11111111111},
                {-99999999999.99999999, -11111111111111.11},
                {253*Math.pow(10, 970), 1},
                {-4.9*Math.pow(10, -324), -1},
//  вот это не должно проходить??? а успешно проходит.
                {01/01/2015,6},
                {0101,-1},
                {000000000000010, 00010000000000.},
                {-.999999999999999999999999999999999999999999999999,.4545}
        };
    }
    @DataProvider
    public Object[][] testDataChar() {
        return new Object[][]{
                {"o","g"},
                {"0","-1"},
                {"//%&?^@",""},
                {""," "},
                {"f","g"},
                {"碳","碳"},
                {"01/01/2015",6},
                {0., "5."}
        };
    }
}
