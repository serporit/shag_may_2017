package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DivTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Div test", dataProvider = "testDataDiv")
    public void divTest(double num1, double num2) {
        double actual = calculator.div(num1, num2);
        double expected = num1/num2;
        Assert.assertEquals(actual, expected, String.format("Error in divTest %s%s !!!", num1, num2));
    }


    @DataProvider
    public Object[][] testDataDiv() {
        return new Object[][]{
                {0, 0},
                {0, 1},
                {1, 0},
                {45654, 3},
                {-999999984.,.0},
                {.018974,.7},
                {565788.44455554455554, },
                {000000000000010, 00010000000000.},
                {253*Math.pow(10, 970), 1},
                {-4.9*Math.pow(10, -324), -1},
                {-.999999999999999999999999999999999999999999999999,.4545},
                {2033/12/01, 88},
                {-0,-1},
                {-.11, 0},
                {"o","g"},
                {"0","-1"},
                {"//%&?^@",""},
                {""," "},
                {"f","g"},
                {"碳","碳"},
                {"01/01/2015",6},
                {0000., "-5888888888855."}
        };
    }
}
