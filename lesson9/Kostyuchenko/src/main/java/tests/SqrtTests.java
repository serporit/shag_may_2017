package tests;
import static java.lang.Math.*;
import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SqrtTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Sqrt test", dataProvider = "testData")
    public void sqrtTest(double num1) {
        double actual = calculator.sqrt(num1);
        double expected = Math.sqrt(num1);
        Assert.assertEquals(actual, expected, String.format("Error in multTest %s%s !!!", num1));
    }


    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {0},
                {1},
                {-1},
                {0.1},
                {45654},
                {-999999988.},
                {.111111111111111},
                {.665658888888},
                {000000000000010},
                {-00010000000000.},
                {253*Math.pow(10, 970)},
                {-4.9*Math.pow(10, -324)},
                {-.009999999999999999999999999999999999999999999999},
                {0000/00/00},
                {-0},
                {-.11},
                {"o"},
                {"0"},
                {"-1"},
                {"//%&?^@"},
                {""},
                {" "},
                {"д"},
                {"碳"},
                {"00/00/1900"},
                {-0000.},
                {"-5888888888855."}
        };
    }
}
