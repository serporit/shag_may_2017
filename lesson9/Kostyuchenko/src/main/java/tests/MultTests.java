package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MultTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Mult test", dataProvider = "testData")
    public void multTest(double num1, double num2) {
        double actual = calculator.mult(num1, num2);
        double expected = num1*num2;
        Assert.assertEquals(actual, expected, String.format("Error in multTest %s%s !!!", num1, num2));
    }


    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {0, 0},
                {0, 1},
                {1, 0},
                {45654, 3},
                {-999999988.,.0},
                {.018974,.7},
                {.665658888888,1},
                {12345679,63},
                {11111,11111},
                {465789532, 1111178987},
                {565788.44455554455554, },
                {000000000000010, 00010000000000.},
                {253*Math.pow(10, 970), 20},
                {-4.9*Math.pow(10, -324), -5.},
                {-.009999999999999999999999999999999999999999999999,.11111111111},
                {0000/00/00, 0},
                {-0,-1},
                {-.11, 0},
                {"o","g"},
                {"0","-1"},
                {"//%&?^@",""},
                {""," "},
                {"l","д"},
                {"碳","碳"},
                {"00/00/1900",6},
                {0000., "-5888888888855."}
        };
    }
}