package bo;

/**
 * Created by Klim_Starykau on 6/12/2017.
 */
public class Calculator {
    public Calculator() {
    }

    public double sum(double a, double b) {
        return a + b;
    }

    public double sub(double a, double b) {
        return a - b;
    }

    public double mult(double a, double b) {
        return a * Math.abs(b);
    }

    public double div(double a, double b) {
        return a / b;
    }

    public double sqr(double a) {
        return Math.pow(a, Math.floor(2));
    }

    public double sqrt(double a) {
        return Math.sqrt(Math.abs(a));
    }
}
