package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SqrTests {
        Calculator calculator;

        @BeforeClass
        public void init() {
            calculator = new Calculator();
        }

        @Test(description = "Sqr test", dataProvider = "testData")
        public void multTestDemo(double num1, double num2) {
            double actual = calculator.sqr(num1);
            double expected = num1*num1;
            Assert.assertEquals(actual, expected, "Incorrect work of Sqr method");
        }


        @DataProvider
        public Object[][] testData() {
            return new Object[][]{
                    {4},
                    {-4},
                    {4.0},
                    {+4},
            };
        }
    }

