package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SqrtTests {

        Calculator calculator;

        @BeforeClass
        public void init() {
            calculator = new Calculator();
        }

        @Test(description = "Sqrt test", dataProvider = "testData")
        public void SqrTestDemo(double num1) {
            double actual = calculator.sqrt(num1);
            double expected = Math.sqrt(num1);
            Assert.assertEquals(actual, expected, "Incorrect work of Sqrt method");
        }

    @Test(description = "Sqrt testException",dataProvider = "testData1", expectedExceptions = ArithmeticException.class)
    public void testSqrtOfNegativeNumber(double num1) {
        double actual = calculator.sqrt(num1);
        double expected = Math.sqrt(-1.0);
        Assert.assertEquals(actual, expected, "Incorrect work of Sqrt method");
    }


        @DataProvider
        public Object[][] testData() {
            return new Object[][]{
                    {4},
                    {+4},
                    {4.5},
            };
        }

    @DataProvider
    public Object[][] testData1() {
        return new Object[][]{
                {-4},
                {-4.0}
        };
    }
}


