package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MultTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Mult test", dataProvider = "testData")
    public void multTestDemo(double num1, double num2) {
        double actual = calculator.mult(num1, num2);
        double expected = num1*num2;
        Assert.assertEquals(actual, expected, "Incorrect work of Mult method");
    }


    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {4, 5},
                {-4, 5},
                {4, -5},
                {4.5, -5},
                {4, -5.1},
                {4, 0},
                {4, -0},
                {-0, -0},
                {-0, 0},
                {4, -0.0}
        };
    }
}
