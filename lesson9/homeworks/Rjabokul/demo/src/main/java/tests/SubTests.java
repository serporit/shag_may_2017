package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SubTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Sub test", dataProvider = "testData")
    public void subTestDemo(double num1, double num2) {
        double actual = calculator.sub(num1, num2);
        double expected = num1 - num2;
        Assert.assertEquals(actual, expected, "Incorrect work of sub method");
    }


    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {5, 5},
                {-4, 5},
                {4, -5},
                {4.5, -5},
                {4, -5.1},
                {4, 0},
                {4, -0},
                {-0, -0},
                {-0, 0},
                {4, -0.0}
        };
    }
}