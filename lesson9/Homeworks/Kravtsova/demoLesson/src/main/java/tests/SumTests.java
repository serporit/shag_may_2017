package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SumTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Summ test", dataProvider = "testData")
    public void sumTestDemo(double num1, double num2) {
        double actual = calculator.sum(num1, num2);
        double expected = num1 + num2;
        Assert.assertEquals(actual, expected, "Summ Method doesn't work");
    }
        @DataProvider
        public Object[][] testData() {
            return new Object[][]{
                    {1, 2},
                    {-1, 2},
                    {1, -2},
                    {-1, -2},
                    {0, 0},

            };
        }
}
