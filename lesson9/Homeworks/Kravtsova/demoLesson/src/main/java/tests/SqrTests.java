package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SqrTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Sqr test", dataProvider = "testData")
    public void SqrTestDemo(double num1) {
        double actual = calculator.sqr(num1);
        double expected = num1*num1;
        Assert.assertEquals(actual, expected, "Sqr Method doesn't work");
    }
    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {1},
                {-1},
                {0}
        };
    }
}
