package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SqrtTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Sqrt test", dataProvider = "testData")
    public void SqrTestDemo(double num1) {
        double actual = calculator.sqrt(num1);
        double expected = Math.sqrt(num1);
        Assert.assertEquals(actual, expected, "Sqrt Method doesn't work");
    }

    @Test(description = "Sqrt test Exception", expectedExceptions = ArithmeticException.class)
    public void testDemoWithExceptionSqrt() {
        double actual = calculator.sqrt(-1);
        double expected = Math.sqrt(-1);
        Assert.assertEquals(actual, expected, "Sqrt Method doesn't work");
    }
    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {1},
                {0}
        };
    }
}
