package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SubTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Sub test", dataProvider = "testData")
    public void subTestDemo(double num1, double num2) {
        double actual = calculator.sub(num1, num2);
        double expected = num1 - num2;
        Assert.assertEquals(actual, expected, "Sub Method doesn't work");
    }
    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {1, 2},
                {-1, 2},
                {1, -2},
                {-1, -2},
                {0, 0},
                {-128, 3},
                {0, 127}
        };
    }
}
