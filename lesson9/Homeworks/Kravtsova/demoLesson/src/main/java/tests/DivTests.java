package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DivTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Div test", dataProvider = "testData")
    public void DivTestDemo(double num1, double num2) {
        double actual = calculator.div(num1, num2);
        double expected = num1/num2;
        Assert.assertEquals(actual, expected, "Mult Method doesn't work");
    }

    @Test(description = "Div test Exception", expectedExceptions = ArithmeticException.class)
    public void testDemoWithExceptionDiv() {
        double actual = calculator.div(1, 0);
        double expected = 1 / 0;
        Assert.assertEquals(actual, expected, "Mult Method doesn't work");
    }
    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {1, 2},
                {-1, 2},
                {1, -2},
                {-1, -2},
                {-128, 3},
                {0, 127}

        };
    }
}
