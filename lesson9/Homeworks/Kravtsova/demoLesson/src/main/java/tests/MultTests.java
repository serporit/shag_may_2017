package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MultTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "Mult test", dataProvider = "testData")
    public void MultTestDemo(double num1, double num2) {
        double actual = calculator.mult(num1, num2);
        double expected = num1*num2;
        Assert.assertEquals(actual, expected, "Mult Method doesn't work");
    }
    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {1, 2},
                {-1, 2},
                {1, -2},
                {-1, -2},
                {0, 0},
                {1, 0},
                {-128, 3},
                {0, 127}
        };
    }
}
