package runner;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class CustomTestListener extends TestListenerAdapter {
    private int m_count = 0;

    @Override
    public void onTestFailure(ITestResult tr) {
        System.out.println(++m_count + ". " + tr.getName() + " is failed" + tr.getParameters());
    }

    @Override
    public void onTestSkipped(ITestResult tr) {
        System.out.println(++m_count + ". " + tr.getName() + " is skipped");
    }

    @Override
    public void onTestSuccess(ITestResult tr) {
        System.out.println(++m_count + ". " + tr.getName() + " is passed");
    }
}