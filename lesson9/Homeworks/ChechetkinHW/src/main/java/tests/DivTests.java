package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DivTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "test Div", dataProvider = "arrayDataRight")
    public void divTestData(double num1, double num2) {
        double actual = calculator.div(num1, num2);
        double expected = num1 / num2;
        String message = "Warning!\n We want : "+ expected + "\n We have : " + actual + "\n";
        Assert.assertEquals(actual, expected, message);
    }

    @Test(description = "ssssssss", dataProvider = "arrayDataIncorrect")
    public void divTestS(double ferst, double second){
        Double actual = calculator.div(ferst, second);
        Double expected = ferst / second;
        String message = "Warning!\n We want : "+ expected + "\n We have : " + actual + "\n";
        Assert.assertFalse(actual.equals(expected), message);
    }

    @Test(description = "Div test", expectedExceptions = ArithmeticException.class)
    public void testDemoWithException() {
        double i = 3.0 /0;
        System.out.println(i);
    }

    @DataProvider
    public Object[][] arrayDataIncorrect(){
        return new Object[][]{
                {'a', 'a'},
                {"s", 4}
        };
    }

    @DataProvider
    public Object[][] arrayDataRight() {
        return new Object[][]{
                {4, 2},
                {4.0, 2.0},
                {0, -0},
                {-0, 0},
                {3, 3},
                {-3, 3},
                {3, -3},
                {-3, -3},
                {3, 0},
                {-3, 0},
                {0, 3},
                {0, -3},
                {-128, -1}, //byte
                {127, 1}, //byte
                {-32768, -1}, // short
                {32767, 1}, // short
                {-2147483648, -1}, //int
                {2147483647, 1}, //int
//                {-9223372036854775807, -1}, // long
//                {9223372036854775807, 1}, // long
                {-1.4e-45f, -1}, // float
                {3.4e+38f, 1}, // float
                {-4.9e-324, -1}, //double
                {1.7e+308, 1}, //double
        };
    }

}
