package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;

public class SqrTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "test Sqr", dataProvider = "arrayVars")
    public void sqrTestData(double num1) {
        double actual = calculator.sqr(num1);
        double expected = num1 * num1;
        String message = "Warning!\n We want : " + expected + "\n we have :" + actual;
        Assert.assertEquals(actual, expected, message);
    }

    @DataProvider
    public Object[][] arrayVars() {
        return new Object[][]{
                {0, -0},
                {-0, 0},
                {3, 3},
                {-3, 3},
                {3, -3},
                {-3, -3},
                {3, 0},
                {-3, 0},
                {0, 3},
                {0, -3},
                {-128, -1}, //byte
                {127, 1}, //byte
                {-32768, -1}, // short
                {32767, 1}, // short
                {-2147483648, -1}, //int
                {2147483647, 1}, //int
//                {-9223372036854775808, -1}, // long
//                {9223372036854775807, 1}, // long
                {-1.4e-45f, -1}, // float
                {3.4e+38f, 1}, // float
                {-4.9e-324, -1}, //double
                {1.7e+308, 1}, //double
        };
    }
}
