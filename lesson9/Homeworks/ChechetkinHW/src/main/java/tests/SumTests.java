package tests;

import bo.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SumTests {
    Calculator calculator;

    @BeforeClass
    public void init() {
        calculator = new Calculator();
    }

    @Test(description = "test Sum", dataProvider = "arrayVars")
    public void sumTestData(double num1, double num2) {
        Assert.assertEquals(calculator.sum(num1, num2), num1 + num2, "Warning!\n We want : " +
                "" + (num1 + num2) + "\n we have " + calculator.sum(num1, num2));
    }

    @DataProvider
    public Object[][] arrayVars() {
        return new Object[][]{
                {0, -0},
                {-0, 0},
                {3, 3},
                {-3, 3},
                {3, -3},
                {-3, -3},
                {3, 0},
                {-3, 0},
                {0, 3},
                {0, -3},
                {-128, -1}, //byte
                {127, 1}, //byte
                {-32768, -1}, // short
                {32767, 1}, // short
                {-2147483648, -1}, //int
                {2147483647, 1}, //int
//                {-9223372036854775808, -1}, // long
//                {9223372036854775807, 1}, // long
                {-1.4e-45f, -1}, // float
                {3.4e+38f, 1}, // float
                {-4.9e-324, -1}, //double
                {1.7e+308, 1}, //double
        };
    }
}
