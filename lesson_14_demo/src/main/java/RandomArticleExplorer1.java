import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;

public class RandomArticleExplorer1 {

    private static final String startUrl = "https://en.wikipedia.org/wiki/";
    private static final By randomPage = By.cssSelector("li#n-randompage");
    private static final By heading = By.xpath("//*[@id='firstHeading']");
    private static final String linkInMainArticlePattern = "(//*/p/a[starts-with(@href,'/wiki/')][0]|//p/a[starts-with(@href,'/wiki/')][0])";
    private static WebDriver driver;
    private static Set<String> articlesLinks = new HashSet();


    @BeforeClass
    private void browserPrepare() {
        System.setProperty("webdriver.chrome.driver", "D:\\Selenium\\chromedriver.exe");
        driver = new ChromeDriver();

        //System.setProperty("webdriver.gecko.driver","D:\\Selenium\\geckodriver.exe");
        //driver = new FirefoxDriver();

        //System.setProperty("webdriver.ie.driver","D:\\Selenium\\IEDriverServer.exe");
        //driver = new InternetExplorerDriver();
    }

    @Test(description = "Test if any random article linked to Philosophy")
    public void wikiTest() {
        driver.get(startUrl);
        driver.findElement(randomPage).click();
        int n=0;
        do {
            openNextLink();
            n++;
        } while (!getArticlename().equals("Philosophy"));
        System.out.println("\nPhilosophy was reached in "+n+" iterations");
        Assert.assertTrue(n<40,"Path longer that 40 steps! ");
    }

    @AfterClass
    private void teardown(){
        driver.quit();
    }

    private static String getArticlename() {
        return driver.findElement(heading).getText();
    }

    private static void openNextLink() {
        int nthLink = 0;
        String nextLink;
        int articlesAmount = articlesLinks.size();
        do {
            nthLink++;
            nextLink = driver.findElement(By.xpath(linkInMainArticlePattern.replaceAll("0", "" + nthLink))).getAttribute("href");
            articlesLinks.add(nextLink);
        } while (articlesAmount == articlesLinks.size());
        driver.navigate().to(nextLink);
        System.out.println(getArticlename());
    }
}
