import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RandomArticleExplorer3 {

    private static final String START_URL = "https://ru.wikipedia.org/wiki/";
    private static final By SEARCH_TEXT_BOX = By.cssSelector("input#searchInput");
    private static final By HEADING = By.xpath("//*[@id='firstHeading']");
    private static final String LINK_IN_MAIN_ARTICLE_PATTERN = "(//*/p/a[starts-with(@href,'/wiki/')][0]|//p/a[starts-with(@href,'/wiki/')][0])";
    private static WebDriver driver;
    private static final String STANDALONE_SERVER_URL = "http://127.0.0.1:4444/wd/hub";
    private static Map<WebDriver,Boolean> browserPool = new HashMap<>();


    @BeforeSuite
    private void browserPrepare() throws MalformedURLException{
        DesiredCapabilities capability;
//        for (int i=0;i<2;i++) {
//            capability = DesiredCapabilities.firefox();
//            browserPool.put(new RemoteWebDriver(new URL(STANDALONE_SERVER_URL),capability),true);
//        }
//        for (int i=0;i<3;i++) {
//            capability = DesiredCapabilities.edge();
//            browserPool.put(new RemoteWebDriver(new URL(STANDALONE_SERVER_URL),capability),true);
//        }
        for (int i=0;i<3;i++) {
            capability = DesiredCapabilities.chrome();
            browserPool.put(new RemoteWebDriver(new URL(STANDALONE_SERVER_URL),capability),true);
        }
    }

    @Parameters({"initialSearch", "maxIterations"})
    @Test(dataProvider="testData", description = "Test if any random article linked to Math")
    public void wikiTest(String initialSearch, int maxIterations) {
        Set<String> articlesLinks = new HashSet();
        WebDriver driver = getFreeBrowser();
        driver.get(START_URL);
        driver.findElement(SEARCH_TEXT_BOX).sendKeys(initialSearch);
        driver.findElement(SEARCH_TEXT_BOX).submit();
        int n=0;
        do {
            openNextLink(driver,articlesLinks);
            n++;
        } while (!getArticlename(driver).equals("Математика"));
        System.out.println("\nMath was reached in "+n+" iterations");
        Assert.assertTrue(n<maxIterations,"Path longer that "+maxIterations+" steps! ");
    }

    @AfterSuite
    private void teardown(){
        for (WebDriver browser:browserPool.keySet()) {browser.quit();}
    }

    private static String getArticlename(WebDriver driver) {
        return driver.findElement(HEADING).getText();
    }

    private static void openNextLink(WebDriver driver, Set<String> articlesLinks) {
        int nthLink = 0;
        String nextLink;
        int articlesAmount = articlesLinks.size();
        do {
            nthLink++;
            nextLink = driver.findElement(By.xpath(LINK_IN_MAIN_ARTICLE_PATTERN.replaceAll("0", "" + nthLink))).getAttribute("href");
            articlesLinks.add(nextLink);
        } while (articlesAmount == articlesLinks.size());
        driver.navigate().to(nextLink);
        System.out.println(getArticlename(driver));
    }

    private static WebDriver getFreeBrowser(){
        WebDriver freeBrowser = null;
        for (WebDriver browser:browserPool.keySet()) {
            if (browserPool.get(browser)) {
                freeBrowser=browser;
                browserPool.put(browser,false);
                break;
            }
        }
        if (freeBrowser==null) {throw new RuntimeException("Browser pool is out of browsers!");}
        return freeBrowser;
    }

    @DataProvider(name="testData", parallel = true)
    public static Object[][] getTestData() {
        return new Object[][]{ {"Сибирь", 45} , {"Мрамор", 30}, {"Охота", 50}, {"Фазан", 50}, {"Радуга", 50}, {"Владивосток", 45}, {"Инфляция", 35}, {"Автоматизация", 30}, {"Тестирование", 30} };
    }

}
