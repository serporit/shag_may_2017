import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class RandomArticleExplorer2 {

    private static final String START_URL = "https://ru.wikipedia.org/wiki/";
    private static final By SEARCH_TEXT_BOX = By.cssSelector("input#searchInput");
    private static final By HEADING = By.xpath("//*[@id='firstHeading']");
    private static final String LINK_IN_MAIN_ARTICLE_PATTERN = "(//*/p/a[starts-with(@href,'/wiki/')][0]|//p/a[starts-with(@href,'/wiki/')][0])";
    private static WebDriver driver;
    private static Set<String> articlesLinks = new HashSet();
    private static final String STANDALONE_SERVER_URL = "http://127.0.0.1:4444/wd/hub";
    private static final String INITIAL_SEARCH = "Эксперимент";


    @BeforeClass
    private void browserPrepare() throws MalformedURLException{
        //DesiredCapabilities capability = DesiredCapabilities.firefox();
        //DesiredCapabilities capability = DesiredCapabilities.edge();
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        driver = new RemoteWebDriver(new URL(STANDALONE_SERVER_URL),capability);
    }

    @Test(description = "Test if any random article linked to Math")
    public void wikiTest() {
        driver.get(START_URL);
        driver.findElement(SEARCH_TEXT_BOX).sendKeys(INITIAL_SEARCH);
        driver.findElement(SEARCH_TEXT_BOX).submit();
        int n=0;
        do {
            openNextLink();
            n++;
        } while (!getArticlename().equals("Математика"));
        System.out.println("\nMath was reached in "+n+" iterations");
        Assert.assertTrue(n<45,"Path longer that 45 steps! ");
    }

    @AfterClass
    private void teardown(){
        driver.quit();
    }

    private static String getArticlename() {
        return driver.findElement(HEADING).getText();
    }

    private static void openNextLink() {
        int nthLink = 0;
        String nextLink;
        int articlesAmount = articlesLinks.size();
        do {
            nthLink++;
            nextLink = driver.findElement(By.xpath(LINK_IN_MAIN_ARTICLE_PATTERN.replaceAll("0", "" + nthLink))).getAttribute("href");
            articlesLinks.add(nextLink);
        } while (articlesAmount == articlesLinks.size());
        driver.navigate().to(nextLink);
        System.out.println(getArticlename());
    }
}
