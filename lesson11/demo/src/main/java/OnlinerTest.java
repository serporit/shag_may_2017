import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OnlinerTest {
    private static final By FIRST_ITEM_LINK_LOCATOR = By.xpath("(//a[@class='product__title-link'])[1]");
    private static final By SEARCH_INPUT_LOCATOR = By.cssSelector(".fast-search__input");
    public static final By RESULTS_FRAME_LOCATOR = By.xpath("//iframe[@src='/sdapi/catalog/search/iframe']");
    private By PRICE_LOCATOR = By.cssSelector(".offers-description__link.offers-description__link_subsidiary");
    WebDriver driver;

    @BeforeClass
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "check that min price of first Iphone SE is lower than 1000 BYN")
    public void openOnliner() {
        driver.get("https://onliner.by");
        driver.findElement(SEARCH_INPUT_LOCATOR).sendKeys("Iphone SE");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(RESULTS_FRAME_LOCATOR));
        driver.switchTo().frame(driver.findElement(RESULTS_FRAME_LOCATOR));
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(FIRST_ITEM_LINK_LOCATOR));
        driver.findElement(FIRST_ITEM_LINK_LOCATOR).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(PRICE_LOCATOR));
        String price = driver.findElement(PRICE_LOCATOR).getText();
        System.out.println("price: " + price);
        double minPrice = Integer.parseInt(price.substring(0, price.indexOf(',')));
        Assert.assertTrue(minPrice < 1000);
    }

    /*
    * Написать такие же тесты для:
    * - Amazon.com (цена первого Iphone SE ниже 400$)
    * - Banana song (Количество просмотров больше 50М)
    * - Yandex mail (Позитивный тест на логин (завести тестовый ящик)) Критерий - вверху справа есть ваш аккаунт
    *    + негативный тест (вводим неправильные данные, проверяем сообщение)
    * */

    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}
