import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.regex.Pattern;

public class BananaSongTest {
    private static final By SEARCH_INPUT_LOCATOR = By.cssSelector("#masthead-search-term");
    private static final By SEARCH_CLICK_LOCATOR = By.cssSelector("#search-btn");
    private static final By SEARCH_QUALITY_VIEWS_LOCATOR = By.xpath(".//*[@id='results']/ol/li[2]/ol/li[1]//ul[@class='yt-lockup-meta-info']/li[2]");

    WebDriver driver;

    @BeforeTest
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "check that quality of views more than 5000000")
    public void testBananaSong() {
        driver.get("https://www.youtube.com");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(SEARCH_INPUT_LOCATOR));
        driver.findElement(SEARCH_INPUT_LOCATOR).sendKeys("Banana song");
        driver.findElement(SEARCH_CLICK_LOCATOR).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(SEARCH_QUALITY_VIEWS_LOCATOR));
        String views = driver.findElement(SEARCH_QUALITY_VIEWS_LOCATOR).getText();

        Pattern pattern = Pattern.compile(",");
        String[] arrayOfString = pattern.split(views);
        StringBuilder oneStringOfArrayString = new StringBuilder();
        for (String temp : arrayOfString) {
            oneStringOfArrayString.append(temp);
        }
        String subString = oneStringOfArrayString.substring(0, oneStringOfArrayString.length() - 6);
        int numberOfViews = Integer.valueOf(subString);

        System.out.println(numberOfViews);

        Assert.assertTrue(numberOfViews >= 5000000);
    }

    @AfterTest
    public void closeBrowser() {
        driver.close();
    }
}