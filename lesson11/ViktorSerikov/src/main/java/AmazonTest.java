import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AmazonTest {
    private static final By FIRST_ITEM_LINK_LOCATOR = By.cssSelector("div#nav-search input.nav-input");
    private static final By SEARCH_INPUT_LOCATOR = By.cssSelector("input#twotabsearchtextbox");
    WebDriver driver;
    private By PRICE_LOCATOR = By.cssSelector("li#result_0 span.sx-price-whole");

    @BeforeClass
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "check that price of the first Iphone not lower than 400 USD")
    public void testAmazon() {
        driver.get("https://amazon.com");
        driver.findElement(SEARCH_INPUT_LOCATOR).sendKeys("Iphone SE");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(FIRST_ITEM_LINK_LOCATOR));
        driver.findElement(FIRST_ITEM_LINK_LOCATOR).click();
        String price = driver.findElement(PRICE_LOCATOR).getText();
        System.out.println("current price: " + price);
        int minPrice = Integer.parseInt(price);
        Assert.assertTrue(minPrice < 400);
    }

    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}
