import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class YandexMailTest {
    private static final String LOGIN = "pupckin.test2017@yandex.ru";
    private static final String PASSWORD = "lesson11";
    private static final String WRONG_PASSWORD = "lesson";

    private static final By LOGIN_INPUT_LOCATOR = By.xpath(".//*[@class='input__box']/input[@placeholder='Логин']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath(".//*[@class='input__box']/input[@placeholder='Пароль']");
    private static final By SEARCH_CLICK_ENTER_LOCATOR = By.xpath("//button[@tabindex='106']");
    private static final By SEARCH_LOGIN_LOCATOR_IN_PROFILE = By.xpath(".//div[@class='mail-User-Name']");
    private static final By SEARCH_ALERT = By.xpath(".//*[@id='root']//div[@data-reactid='14']");

    WebDriver driver;

    @BeforeTest
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "verify Yandex.ru mail forms [login] and [password] with INcorrect data ")
    public void testYandexMailWithIncorrectData() {

        driver.get("https://www.yandex.com/");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(LOGIN_INPUT_LOCATOR));
        driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(LOGIN);
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(WRONG_PASSWORD);
        driver.findElement(SEARCH_CLICK_ENTER_LOCATOR).click();

        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(SEARCH_ALERT));
        driver.findElement(SEARCH_ALERT);

    }

    @Test(description = "verify Yandex.ru mail forms [login] and [password] with correct data ")
    public void testYandexMailWithCorrectData() {

        driver.get("https://www.yandex.com/");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(LOGIN_INPUT_LOCATOR));
        driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(LOGIN);
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(PASSWORD);
        driver.findElement(SEARCH_CLICK_ENTER_LOCATOR).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(SEARCH_LOGIN_LOCATOR_IN_PROFILE));
        String login = driver.findElement(SEARCH_LOGIN_LOCATOR_IN_PROFILE).getText();

        Assert.assertTrue(LOGIN.equals(login + "@yandex.ru"));
    }

    @AfterTest
    public void closeBrowser() {
        driver.close();
    }
}


