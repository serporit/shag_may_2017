import org.apache.commons.lang3.ObjectUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

public class HomeTests {
    public static final By LOCATOR_SEARH_FIELD = By.xpath(".//*[@id='twotabsearchtextbox']");
    public static final By LOCATOR_BUTTON_GO = By.xpath("//input[@value='Go']");
    public static final By VERY_BAD_LOCATOR_FOR_PRICE = By.xpath("//*[@id='result_0']/div/div/div/div[2]/div[3]/div[1]/div[1]/a/span/span/span");
    private WebDriver driver;

    @BeforeTest
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "check that price of first Iphone SE is lower than 400")
    public void testAmazon() {
        driver.get("https://amazon.com");
        driver.findElement(LOCATOR_SEARH_FIELD).sendKeys("Iphone SE");
        driver.findElement(LOCATOR_BUTTON_GO).click();
        Integer price = Integer.parseInt(driver.findElement(VERY_BAD_LOCATOR_FOR_PRICE).getText());
        Assert.assertTrue(price < 400);
    }

    @Test(description = "check that wievs more than 50M")
    public void testBanana() {
        driver.get("https://youtube.com");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='masthead-search-term']")));
        driver.findElement(By.xpath("//input[@id='masthead-search-term']")).sendKeys("Banana song");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='search-btn']")));
        driver.findElement(By.xpath("//button[@id='search-btn']")).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Minions Banana Song Full Song']")));
        driver.findElement(By.xpath("//a[@title='Minions Banana Song Full Song']")).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//video")));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//video")).click();
        String wievs = driver.findElement(By.cssSelector(".watch-view-count")).getText();
        Integer wievsInt = Integer.parseInt(wievs.replaceAll("\\D", ""));
        Assert.assertTrue(wievsInt > 50000000, "We have " + wievsInt + " wievs\n");
    }

    @Test (description = "Try login on site Yandex mail with +log and +pass")
    public void testPosYandexMail(){
        driver.get("https://yandex.by/");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Логин']")));
        driver.findElement(By.xpath("//input[@placeholder='Логин']")).sendKeys("p.a.chechetkin");
        driver.findElement(By.xpath("//input[@placeholder='Пароль']")).sendKeys("agentbars0007");
        driver.findElement(By.xpath("//button[@tabindex='106']")).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".mail-User-Name")));

        Assert.assertTrue(driver.findElement(By.cssSelector(".mail-User-Name")).isDisplayed(),"Not wiev login name");
    }

    @Test (description = "Try login on site Yandex mail with +log and -pass")
    public void testNegYandexMail(){
        driver.get("https://yandex.by/");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Логин']")));
        driver.findElement(By.xpath("//input[@placeholder='Логин']")).sendKeys("p.a.chechetkin");
        driver.findElement(By.xpath("//input[@placeholder='Пароль']")).sendKeys("12345");
        driver.findElement(By.xpath("//button[@tabindex='106']")).click();

        Assert.assertTrue(driver.findElement(By.cssSelector(".passport-Domik-Form-Error")).isDisplayed(), "No Allert Message");
    }

    /*
    * Написать такие же тесты для:
    * - Amazon.com (цена первого Iphone SE ниже 400$)
    * - Banana song (Количество просмотров больше 50М)
    * - Yandex mail (Позитивный тест на логин (завести тестовый ящик)) Критерий - вверху справа есть ваш аккаунт
    *    + негативный тест (вводим неправильные данные, проверяем сообщение)
    * */

    @AfterTest
    public void closeBrowser() {
        driver.close();
    }
}
