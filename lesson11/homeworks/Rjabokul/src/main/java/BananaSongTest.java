import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.regex.Pattern;

public class BananaSongTest {

    public static final By SEARCH_FIELD_LOCATOR = By.xpath("//input[@id='masthead-search-term']");
    public static final By SEARCH_BUTTON_LOCATOR = By.xpath("//button[@id='search-btn']");
    public static final By BANANA_SONG_LINK_LOCATOR = By.xpath("//a[@title='Minions Banana Song Full Song']");
    private static final By VIEWS_LOCATOR = By.cssSelector(".watch-view-count");

    WebDriver driver;

    @BeforeTest
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "check that quantity of views more than 50m")
    public void bananaSongTest() {
        driver.get("https://youtube.com");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(SEARCH_FIELD_LOCATOR));
        driver.findElement(SEARCH_FIELD_LOCATOR).sendKeys("Banana song");
        driver.findElement(SEARCH_BUTTON_LOCATOR).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(BANANA_SONG_LINK_LOCATOR));
        driver.findElement(BANANA_SONG_LINK_LOCATOR).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(VIEWS_LOCATOR));
        String views = driver.findElement(VIEWS_LOCATOR).getText();
        int numberOfViews = Integer.parseInt(views.replaceAll("\\D", ""));
        System.out.println(numberOfViews);
        Assert.assertTrue(numberOfViews >= 50000000);
    }

    @AfterTest
    public void closeBrowser() {
        driver.close();
    }
}