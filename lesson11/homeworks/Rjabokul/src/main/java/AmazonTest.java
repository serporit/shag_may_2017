import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AmazonTest {
    private static final By SEARCH_INPUT_LOCATOR = By.xpath("//*[@id='twotabsearchtextbox']");
    private static final By SEARCH_BUTTON = By.xpath("//input[@value='Go']");
    private By PRICE_LOCATOR = By.cssSelector("li#result_0 span.sx-price-whole");
    WebDriver driver;

    @BeforeClass
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "check that price of the first Iphone lower than 400 USD")
    public void amazonTest() {
        driver.get("https://amazon.com");
        driver.findElement(SEARCH_INPUT_LOCATOR).sendKeys("Iphone SE");
        driver.findElement(SEARCH_BUTTON).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(PRICE_LOCATOR));
        String price = driver.findElement(PRICE_LOCATOR).getText();
        System.out.println("price: " + price);
        int currentPrice = Integer.parseInt(price);
        Assert.assertTrue(currentPrice < 400);
    }

    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}
