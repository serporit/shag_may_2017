import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class BananaSongTest {
    private static final By SEARCH_LOCATOR = By.cssSelector("#masthead-search-term");
    private static final By SEARCH_BUTTON_LOCATOR = By.cssSelector("#search-btn");
    private static final By QUANTITY_VIEWS_LOCATOR = By.cssSelector(".watch-view-count");
    private static final By BANANA_SONG_LOCATOR =By.xpath("//a[@title='Minions Banana Song Full Song']");

    WebDriver driver;

    @BeforeTest
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(description = "check that views are more than 50000000")
    public void testBananaSong() {
        driver.get("https://www.youtube.com");
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(SEARCH_LOCATOR));
        driver.findElement(SEARCH_LOCATOR).sendKeys("Banana song");
        driver.findElement(SEARCH_BUTTON_LOCATOR).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(BANANA_SONG_LOCATOR));
        driver.findElement(BANANA_SONG_LOCATOR).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(QUANTITY_VIEWS_LOCATOR));
        String views = driver.findElement(QUANTITY_VIEWS_LOCATOR).getText();
        int numberOfViews = Integer.parseInt(views.replaceAll("\\D", ""));
        System.out.println(numberOfViews);
        Assert.assertTrue(numberOfViews >= 50000000);
    }

    @AfterTest
    public void closeBrowser() {
        driver.close();
    }
}