import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class YandexTest {
private static final By LOGIN_LOCATOR = By.xpath("//*[@id='nb-1']/span/input");
private static final By PASSWORD_LOCATOR = By.xpath("//*[@id='nb-6']/span/input");
private static final By BUTTON_LOCATOR = By.cssSelector(".nb-button._nb-action-button.nb-group-start");
private static final By USER_LINK_IN_PROFILE = By.cssSelector(".mail-User-Name");
private static final By ALERT_LOCATOR = By.cssSelector(".passport-Domik-Form-Error");
private static final String LOGIN = "olga.kravtsova123";
private static final String PASSWORD = "password123";
private static final String WRONG_PASSWORD = "empty";
WebDriver driver;

        @BeforeTest
        public void preconditions() {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
            }

            @Test(description = "check yandex mail with incorrect data")
    public void testYandexMailWithIncorrectData() {
                driver.get("https://mail.yandex.by/");
                new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(LOGIN_LOCATOR));
                driver.findElement(LOGIN_LOCATOR).sendKeys(LOGIN);
                driver.findElement(PASSWORD_LOCATOR).sendKeys(WRONG_PASSWORD);
                driver.findElement(BUTTON_LOCATOR).click();
                new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(ALERT_LOCATOR));
                Assert.assertTrue(driver.findElement(ALERT_LOCATOR).isDisplayed(), "Alert is not displayed");
                    }

           @Test(description = "check yandex mail with correct data")
    public void positiveYandexTest() {
                driver.get("https://mail.yandex.by/");
                new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(LOGIN_LOCATOR));
                driver.findElement(LOGIN_LOCATOR).sendKeys(LOGIN);
                driver.findElement(PASSWORD_LOCATOR).sendKeys(PASSWORD);
                driver.findElement(BUTTON_LOCATOR).click();
                new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(USER_LINK_IN_PROFILE));
                String login = driver.findElement(USER_LINK_IN_PROFILE).getText();
                Assert.assertTrue(LOGIN.equals(login));
            }

    @AfterTest
    public void closeBrowser() {
        driver.close();
            }
}

