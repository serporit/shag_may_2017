package tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import pages.ComposePage;
import pages.InboxPage;
import pages.LoginPage;
import pages.SentPage;

import java.util.Random;

public class CheckMail extends AbstractTest {
    public static final String LOGIN = "anton.lopanov.1990";
    public static final String PASSWORD = "Kolosa3_40";

    @Test(description = "Send message for myself")
    public void testSendMessage() {

        new LoginPage(driver).open().fillLoginInput(LOGIN).fillPasswordInput(PASSWORD).clickEnter();
        ComposePage composePage = new InboxPage(driver).goToComposePage();
        composePage.setAdress(LOGIN + "@yandex.by");
        Integer random1 = new Random().nextInt();
        Integer random2 = new Random().nextInt();
        composePage.setSubj(String.valueOf(random1));
        composePage.setText(String.valueOf(random2));
        composePage.clickSubmit().waitForEnementVisible(By.xpath("//div[@data-key='view=done']"));
        SentPage sentPage = composePage.goToSentPage();
        sentPage.waitForEnementVisible(By.xpath(".//span[@title=" + Integer.toString(random1) + "]"));
        sentPage.waitForEnementVisible(By.xpath(".//span[@title=" + Integer.toString(random2) + "]"));
        InboxPage inboxPage = sentPage.goToInboxPageWithNewMessage();
        inboxPage.waitForEnementVisible(By.xpath(".//span[@title=" + Integer.toString(random1) + "]"));
        inboxPage.waitForEnementVisible(By.xpath(".//span[@title=" + Integer.toString(random2) + "]"));

    }

}
