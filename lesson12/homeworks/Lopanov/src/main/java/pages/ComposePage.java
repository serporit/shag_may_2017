package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ComposePage extends InterfacePage {

    public static final By SET_ADRESS_LOCATOR = By.xpath(".//div[@name='to']");
    public static final By SET_SABJECT_LOCATOR = By.xpath(".//input[@name='subj']");
    public static final By SET_TEXT_LOCATOR = By.cssSelector(".cke_wysiwyg_div.cke_reset.cke_enable_context_menu.cke_editable.cke_editable_themed.cke_contents_ltr.cke_show_borders");
    public static final By CLICK_SUBMIT = By.xpath("//button[@type='submit']");

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public ComposePage setAdress(String adress) {
        waitForEnementVisible(SET_ADRESS_LOCATOR);
        driver.findElement(SET_ADRESS_LOCATOR).sendKeys(adress);
        return this;
    }

    public ComposePage setSubj(String text) {
        driver.findElement(SET_SABJECT_LOCATOR).sendKeys(text);
        return this;
    }

    public ComposePage setText(String text) {
        driver.findElement(SET_TEXT_LOCATOR).sendKeys(text);
        return this;
    }

    public ComposePage clickSubmit() {
        waitForEnementVisible(CLICK_SUBMIT);
        driver.findElement(CLICK_SUBMIT).click();
        return this;
    }

}
