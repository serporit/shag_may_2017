package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InboxPage extends InterfacePage {

    public static final By COMPOSE_LOCATOR = By.xpath("//a[@href='#compose']//span[@class='mail-ComposeButton-Text']");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public ComposePage goToComposePage() {
        waitForEnementVisible(COMPOSE_LOCATOR);
        driver.findElement(COMPOSE_LOCATOR).click();
        return new ComposePage(driver);
    }

}
