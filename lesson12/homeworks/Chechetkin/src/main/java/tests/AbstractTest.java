package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.LoginPage;

public class AbstractTest {
    public static final String LOGIN = "p.a.chechetkin";
    public static final String PASSWORD = "agentbars0007";
    protected WebDriver driver;

    @BeforeClass
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        new LoginPage(driver).open().fillLoginInput(LOGIN).fillPasswordInput(PASSWORD).clickEnter();
    }

    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}
