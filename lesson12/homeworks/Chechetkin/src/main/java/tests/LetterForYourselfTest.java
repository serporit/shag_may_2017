package tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import pages.ComposePage;
import pages.InboxPage;
import pages.SentPage;

import java.util.Random;

/**
 * Created by Администратор on 01.07.2017.
 */
public class LetterForYourselfTest extends AbstractTest {

    public static final By MESSAGE_THAT_SENT = By.xpath("//div[@data-key='view=done']");

    @Test(description = "Send message for myself")
    public void testSendMessage() {
        ComposePage composePage = new InboxPage(driver).goToComposePage();
        Integer random = new Random().nextInt();
        String adress = "p.a.chechetkin@yandex.by";
        composePage.setAdress(adress).setSubj(String.valueOf(random)).clickSubmit().waitForEnementVisible(MESSAGE_THAT_SENT);
        SentPage sentPage = composePage.goToSentPage();
        sentPage.waitForEnementVisible(By.xpath(".//span[@title=" + Integer.toString(random) + "]"));
        InboxPage inboxPage = sentPage.goToInboxPageWithNewMessage();
        inboxPage.waitForEnementVisible(By.xpath(".//span[@title=" + Integer.toString(random) + "]"));
    }

}
