package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ComposePage extends InterfacePage {

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public ComposePage setAdress(String adress){
        waitForEnementVisible(By.xpath(".//div[@name='to']"));
        driver.findElement(By.xpath(".//div[@name='to']")).sendKeys(adress);
        return this;
    }

    public ComposePage setSubj(String adress){
        driver.findElement(By.xpath(".//input[@name='subj']")).sendKeys(adress);
        return this;
    }

    public ComposePage waitAndClick(By locator){
        waitForEnementVisible(locator);
        driver.findElement(locator).click();
        return this;
    }

    public ComposePage clickSubmit(){
        waitForEnementVisible(By.xpath("//button[@type='submit']"));
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        return this;
    }

}
