package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Администратор on 01.07.2017.
 */
public class InterfacePage extends AllPage {

    public static final By INBOX_LOCATOR = By.xpath("//a[@href='#inbox' and @data-params='fid=1']");
    public static final By SENT_LOCATOR = By.xpath("//a[@href='#sent' and @data-params='fid=4']");
    public static final By TRASH_LOCATOR = By.xpath("//a[@href='#trash' and @data-params='fid=3']");
    public static final By DRAFT_LOCATOR = By.xpath("//a[@href='#draft' and @data-params='fid=6']");
    public static final By SPAM_LOCATOR = By.xpath("//a[@href='#spam' and @data-params='fid=2']");
    public static final By NEW_MESSAGE_LOCATOR = By.xpath(".//a[@href='#inbox?extra_cond=only_new']");

    public InterfacePage(WebDriver driver) {
        super(driver);
    }

    public InboxPage goToInboxPage() {
        waitForEnementVisible(INBOX_LOCATOR);
        driver.findElement(INBOX_LOCATOR).click();
        return new InboxPage(driver);
    }

    public InboxPage goToInboxPageWithNewMessage() {
        waitForEnementVisible(NEW_MESSAGE_LOCATOR);
        driver.findElement(NEW_MESSAGE_LOCATOR).click();
        return new InboxPage(driver);
    }

    public SentPage goToSentPage() {
        waitForEnementVisible(SENT_LOCATOR);
        driver.findElement(SENT_LOCATOR).click();
        return new SentPage(driver);
    }

    public TrashPage goToTrashPage() {
        waitForEnementVisible(TRASH_LOCATOR);
        driver.findElement(TRASH_LOCATOR).click();
        return new TrashPage(driver);
    }

    public DraftsPage goToDraftPage() {
        waitForEnementVisible(DRAFT_LOCATOR);
        driver.findElement(DRAFT_LOCATOR).click();
        return new DraftsPage(driver);
    }

    public SpamPage goToSpamPage() {
        waitForEnementVisible(SPAM_LOCATOR);
        driver.findElement(SPAM_LOCATOR).click();
        return new SpamPage(driver);
    }
}
