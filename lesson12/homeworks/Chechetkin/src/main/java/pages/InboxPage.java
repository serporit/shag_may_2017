package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InboxPage extends InterfacePage {

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public ComposePage goToComposePage(){
        waitForEnementVisible(By.xpath("//a[@href='#compose']//span[@class='mail-ComposeButton-Text']"));
        driver.findElement(By.xpath("//a[@href='#compose']//span[@class='mail-ComposeButton-Text']")).click();
        return new ComposePage(driver);
    }

}
