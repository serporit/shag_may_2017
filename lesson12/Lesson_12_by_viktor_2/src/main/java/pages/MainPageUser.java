package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPageUser extends AbstractPage { private static final By USER_LOGIN_LOCATOR = By.className("mail-User-Name");
    private static final By WRITE_NEW_EMAIL_BUTTON_LOCATOR = By.className("mail-ComposeButton");
    private static final By ADRESS_INPUT_LOCATOR = By.xpath("//div[@name='to']");
    private static final By SEND_MAIL_LOCATOR = By.xpath("//button[@id='nb-20' ]//span[@class ='_nb-button-content']");
    private static final By FIND_THE_SAME_TITLE_IN_MAILBOX = By.xpath(".//*[@title=1277512748]");
    private static final By CLICK_TO_OPEN_USER_SETTINGS_LOCATOR = By.className("mail-User-Name");
    private static final By CLICK_TO_CLICK_EXIT_LOCATOR = By.xpath(".//*[@class ='b-mail-dropdown__item'][last()]");
    private static final By CLICK_TO_INPUT_MAILBOX_LOCATOR = By.xpath("//*[@data-params=\"fid=4\"]");
    private static final By CLICK_TO_OUTPUT_MAILBOX_LOCATOR = By.xpath(".//a[@data-fid=\"4\"]//span[@class=\"mail-NestedList-Item-Name js-folders-item-name\"]");
    private static final By WAIT_LOCATOR_BEFORE_INPUT_MAIBOX = By.xpath("//*[@class=\"mail-Done-Title js-title-info\"]");
    private static final By ARTICLE_INPUT_LOCATOR =  By.xpath(".//*[@name='subj']");

    public MainPageUser(WebDriver driver) {
        super(driver);
    }

    public String getLogin() {
        waitForElementVisible(USER_LOGIN_LOCATOR);
        return driver.findElement(USER_LOGIN_LOCATOR).getText();
    }

    public MainPageUser clickToWriteNewEmail() {
        waitForElementVisible(WRITE_NEW_EMAIL_BUTTON_LOCATOR);
        driver.findElement(WRITE_NEW_EMAIL_BUTTON_LOCATOR).click();
        return new MainPageUser(driver);
    }

    public MainPageUser fillAdress(String login) {
        waitForElementVisible(ADRESS_INPUT_LOCATOR);
        driver.findElement(ADRESS_INPUT_LOCATOR).sendKeys(login);
        return this;
    }

    public MainPageUser fillArticle(Integer login) {
        waitForElementVisible(ARTICLE_INPUT_LOCATOR);
        driver.findElement(ARTICLE_INPUT_LOCATOR).sendKeys(login.toString());
        return this;
    }

    public MainPageUser clickToSendEmail() {
        waitForElementVisible(SEND_MAIL_LOCATOR);
        driver.findElement(SEND_MAIL_LOCATOR).click();
        return this;
    }

    public MainPageUser clickToOpenInputBox() {
        waitForElementVisible(WAIT_LOCATOR_BEFORE_INPUT_MAIBOX);
        driver.findElement(CLICK_TO_INPUT_MAILBOX_LOCATOR).click();
        return this;
    }

    public MainPageUser findTheSameTitleOfLastLetterInInbox(Integer title) {
        waitForElementVisible(By.xpath(".//*[@title=" + title + "]"));
        driver.findElement(FIND_THE_SAME_TITLE_IN_MAILBOX);
        return this;
    }

    public MainPageUser clickToOpenOutputBox() {
        waitForElementVisible(CLICK_TO_OUTPUT_MAILBOX_LOCATOR);
        driver.findElement(CLICK_TO_OUTPUT_MAILBOX_LOCATOR).click();
        return this;
    }

    public MainPageUser findTheSameTitleOfLastLetterInOutbox(Integer title) {
        waitForElementVisible(By.xpath(".//*[@title=" + title + "]"));
        driver.findElement(FIND_THE_SAME_TITLE_IN_MAILBOX);
        return this;
    }

    public MainPageUser clickToOpenUserSettings() {
        waitForElementVisible(CLICK_TO_OPEN_USER_SETTINGS_LOCATOR);
        driver.findElement(CLICK_TO_OPEN_USER_SETTINGS_LOCATOR).click();
        return this;
    }

    public MainPageUser clickToLogOutUser() {
        waitForElementVisible(CLICK_TO_CLICK_EXIT_LOCATOR);
        driver.findElement(CLICK_TO_CLICK_EXIT_LOCATOR).click();
        return this;
    }
}
