package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
    private static final By ENTER_BUTTON_LOCATOR = By.cssSelector("span.new-auth-submit button");
    private static final By ALERT_LOCATOR = By.xpath("//*[contains(@class, 'error-hint') or contains(@class, 'passport-Domik-Form-Error')]");
    private static final String BASE_URL = "https://mail.yandex.com";


    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage open() {
        driver.get(BASE_URL);
        return this;
    }

    public LoginPage fillLoginInput(String login) {
        driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(login);
        return this;
    }

    public LoginPage fillPasswordInput(String password) {
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
        return this;
    }

    public MainPageUser clickEnter() {
        driver.findElement(ENTER_BUTTON_LOCATOR).click();
        return new MainPageUser(driver);
    }

    public boolean isAlertPresent() {
        waitForElementVisible(ALERT_LOCATOR);
        return driver.findElement(ALERT_LOCATOR).isDisplayed();
    }
}

