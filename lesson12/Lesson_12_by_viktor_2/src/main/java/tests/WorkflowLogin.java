package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MainPageUser;

public class WorkflowLogin extends AbstractTest {

    @Test()
    public void testWorkflow() {
        new LoginPage(driver).open().fillLoginInput(LOGIN).fillPasswordInput(PASSWORD).clickEnter();
        Assert.assertEquals(new MainPageUser(driver).getLogin() + "@yandex.ru", LOGIN);
    }
}
