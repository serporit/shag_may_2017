package tests;

import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MainPageUser;

import java.util.Random;

public class WorkflowSendEmail extends AbstractTest {

    @Test(description = "Test verifies workflow of sending and receiving email by title")
    public void sendEmail() {

        MainPageUser mainPageUser = new LoginPage(driver)
                .open()
                .fillLoginInput(LOGIN)
                .fillPasswordInput(PASSWORD)
                .clickEnter()
                .clickToWriteNewEmail();

        Random random = new Random();
        int currentTitle = random.nextInt();

        mainPageUser
                .fillArticle(currentTitle)
                .fillAdress(LOGIN)
                .clickToSendEmail()
                .clickToOpenInputBox()
                .findTheSameTitleOfLastLetterInInbox(currentTitle)
                .clickToOpenOutputBox()
                .findTheSameTitleOfLastLetterInOutbox(currentTitle)
                .clickToOpenUserSettings()
                .clickToLogOutUser();
    }
}
