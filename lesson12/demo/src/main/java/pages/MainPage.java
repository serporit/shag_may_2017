package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends AbstractPage {
    private static final By USER_LOGIN_LOCATOR = By.className("mail-User-Name");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public String getLogin() {
        waitForEnementVisible(USER_LOGIN_LOCATOR);
        return driver.findElement(USER_LOGIN_LOCATOR).getText();
    }

}
