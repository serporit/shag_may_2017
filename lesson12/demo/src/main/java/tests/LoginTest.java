package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MainPage;

public class LoginTest extends AbstractTest {
    private static final String LOGIN = "pupckin.test2017@yandex.ru";
    private static final String PASSWORD = "lesson11";

    @Test(description = "Positive login test")
    public void testYandexMailWithCorrectData() {
//        LoginPage loginPage = new LoginPage(driver);
//        loginPage.open();
//        loginPage.fillLoginInput(LOGIN);
//        loginPage.fillPasswordInput(PASSWORD);
//        loginPage.clickEnter();
//        MainPage mainPage = new MainPage(driver);
//        String userLogin = mainPage.getLogin()+"@yandex.ru";
//        Assert.assertEquals(LOGIN, userLogin);

        MainPage mainPage = new LoginPage(driver).open().fillLoginInput(LOGIN).fillPasswordInput(PASSWORD).clickEnter();
        Assert.assertEquals(LOGIN, mainPage.getLogin()+ "@yandex.ru");
    }
}


