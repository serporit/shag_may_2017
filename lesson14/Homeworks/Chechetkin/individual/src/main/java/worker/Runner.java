package worker;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.YandexDiskPage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Администратор on 02.07.2017.
 */
public class Runner {


    protected WebDriver driver;

    @BeforeTest
    public void preconditions() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }


    @Test
    public void ferst() throws IOException, InterruptedException {
        YandexDiskPage yandexDiskPage = new YandexDiskPage(driver);
        yandexDiskPage.open().enterLogin().enterPass().clickSubmit();

    }

}
