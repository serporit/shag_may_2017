package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YandexDiskPage {
    public static final By BUTTON_SUBMIT_LOCATOR = By.xpath(".//button[@type='submit']");
    protected WebDriver driver;

    public static final By LOGIN_LOCATOR = By.name("login");
    public static final By PASS_LOCATOR = By.name("password");

    public YandexDiskPage(WebDriver driver) {
        this.driver = driver;
    }

    protected void wayter(By locator){
        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public YandexDiskPage open() {
        driver.get("https://disk.yandex.by");
        return new YandexDiskPage(driver);
    }

    public YandexDiskPage enterLogin(){
        wayter(LOGIN_LOCATOR);
        Actions actions = new Actions(driver);
        actions.sendKeys(driver.findElement(LOGIN_LOCATOR),"p.a.chechetkin").build().perform();
        return new YandexDiskPage(driver);
    }

    public YandexDiskPage enterPass(){
        wayter(PASS_LOCATOR);
        Actions actions = new Actions(driver);
        actions.sendKeys(driver.findElement(PASS_LOCATOR),"agentbars0007").build().perform();;
        return new YandexDiskPage(driver);
    }

    public YandexDiskPage clickSubmit(){
        wayter(BUTTON_SUBMIT_LOCATOR);
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(BUTTON_SUBMIT_LOCATOR)).build().perform();;
        return new YandexDiskPage(driver);
    }

}
